Database of Abstracts of Reviews of Effects (DARE)
==================================================

The [Centre for Reviews and Dissemination (CRD)](http://www.york.ac.uk/inst/crd/about_us.htm) is an international center engaged exclusively in evidence synthesis in the health field. The CRD also manages a [number of databases](http://www.crd.york.ac.uk/CRDWeb/) that are used extensively by health professionals, policy makers and researchers around the world. The CRD also undertakes methods research and produces internationally accepted guidelines for undertaking systematic reviews.

The **Database of Abstracts of Reviews of Effects (DARE)**: Between 1994 and March 2015, CRD researchers systematically searched the world literature to identify and describe systematic reviews, appraise their quality and highlight strengths and weaknesses.

[Extensive search strategies](http://www.crd.york.ac.uk/crdweb/searchstrategies.asp) were run on a weekly basis to identify potential systematic reviews.

Those citations identified as potential systematic reviews were then independently assessed for inclusion by two researchers using the following criteria:

1.  Were inclusion/exclusion criteria reported?
2.  Was the search adequate?
3.  Were the included studies synthesized?
4.  Was the quality of the included studies assessed?
5.  Are sufficient details about the individual included studies presented?

To be included, reviews had to meet at least four of those criteria.

*PubMed Health*
 *10 May 2016*


