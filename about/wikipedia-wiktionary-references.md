Bibliographic Citations for Definitions from [Wikipedia](#Wikipedia) and [Wiktionary](#Wiktionary)
==================================================================================================

<span>These citations follow Wikipedia's recommended </span>[MLA style](http://en.wikipedia.org/wiki/Wikipedia:Citing_Wikipedia#MLA_style)<span>.</span>

<span>Wikipedia</span>
----------------------

1.  Wikipedia contributors. “ACE inhibitor.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 Dec 2016. Web. 24 Jan 2017.

2.  Wikipedia contributors. “Abdominal surgery.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 7 Oct 2013. Web. 10 Jun 2014.

3.  Wikipedia contributors. “Acne vulgaris.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jul 2015. Web. 7 Jul 2015.

4.  Wikipedia contributors. “Adhesion (medicine).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 29 Aug 2015. Web. 4 Dec 2015.

5.  Wikipedia contributors. “Adhesive capsulitis of shoulder.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Sep 2015. Web. 4 Dec 2015.

6.  Wikipedia contributors. “Adjustable gastric band.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Apr 2014. Web. 29 Apr 2014.

7.  Wikipedia contributors. “Adrenal gland disorder.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Mar 2016. Web. 25 Oct 2016.

8.  Wikipedia contributors. “Adrenergic receptor.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Sep 2015. Web. 18 Sep 2015.

9.  ­­­­­Wikipedia contributors. “Amblyopia.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 15 Nov 2015. Web. 24 Nov 2015.

10. <span>Wikipedia contributors. “Anatomical terms of motion.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 23 May 2014. Web. 27 May 2014.</span>

11. <span>Wikipedia contributors. “Anatomy.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, </span><span>24 Apr 2016. Web. 29 Apr 2016.</span>

12. Wikipedia contributors. “Angiotensin-converting enzyme.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Mar 2015. Web. 18 Sep 2015.

13. Wikipedia contributors. “Angiotensin receptor.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 4 Aug 2014. Web. 23 Sep 2015.

14. Wikipedia contributors. “Angiotensin II receptor antagonist.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 Sep 2015. Web. 18 Sep 2015.

15. <span>Wikipedia contributors. “Ankle fracture.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 20 Mar 2015. Web. 29 Apr 2015.</span>

16. Wikipedia contributors. “Anomic aphasia.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Mar 2016. Web. 4 Mar 2016.

17. Wikipedia contributors. “Anorexia nervosa.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 9 Nov 2015. Web. 17 Nov 2015.

18. Wikipedia contributors. “Antihypertensive drug.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 29 Jun 2015. Web. 7 Aug 2015.

19. Wikipedia contributors. “Aorta.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 Aug 2016. Web. 5 Oct 2016.

20. <span>Wikipedia contributors. “Apraxia.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 21 Aug 2015. Web. 2 Sep 2015.</span>

21. Wikipedia contributors. “Aromatherapy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Apr 2015. Web. 27 Apr 2015.

22. Wikipedia contributors. “Articular disk.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 9 Jun 2015. Web. 1 Jul 2015.

23. <span>Wikipedia contributors. “Atopy.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 3 Feb 2015. Web. 27 Apr 2015.</span>

24. <span>Wikipedia contributors. “Aura (symptom).” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 15 Feb 2015. Web. 27 Mar 2015.</span>

25. Wikipedia contributors. “Ball and socket joint.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 17 May 2014. Web. 27 May 2014.

26. Wikipedia contributors. “Bariatric surgery.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 Apr 2014. Web. 29 Apr 2014.

27. Wikipedia contributors. “Beta blocker.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 7 Sep 2015. Web. 18 Sep 2015.

28. Wikipedia contributors. “Blind spot.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 9 Dec 2013. Web. 23 May 2014.

29. Wikipedia contributors. “Bone density.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Mar 2015. Web. 17 Mar 2015.

30. Wikipedia contributors. “Bone disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 May 2016. Web. 3 Oct 2016.

31. Wikipedia contributors. “Bone marrow.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 14 May 2015. Web. 29 May 2015.

32. Wikipedia contributors. “Burnout (psychology).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Apr 2015. Web. 21 Apr 2015.

33. Wikipedia contributors. “Calculus (dental).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 31 May 2014. Web. 16 July 2014.

34. Wikipedia contributors. “Candidiasis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Feb 2015. Web. 2 Mar 2015.

35. Wikipedia contributors. “Canine tooth.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 June 2014. Web. 14 July 2014.

36. Wikipedia contributors. “Carpal tunnel.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 29 May 2014. Web. 1 Jul 2014.

37. Wikipedia contributors. “Cataract surgery.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 6 Apr 2015. Web. 30 Apr 2015.

38. Wikipedia contributors. “Cauda equina.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Aug 2014. Web. 24 Feb 2015.

39. Wikipedia contributors. “Cervical cap.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 4 Mar 2014. Web. 30 Apr 2014.

40. Wikipedia contributors. “Chlorophyll.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 22 May 2014. Web. 28 May 2014.

41. Wikipedia contributors. “Chronic kidney disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Dec 2015. Web. 14 Dec 2015.

42. Wikipedia contributors. “Chronic wound.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Jan 2016. Web. 1 Feb 2016.

43. Wikipedia contributors. “Clitoral hood.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 4 Dec 2015. Web. 1 Feb 2016.

44. Wikipedia contributors. “Clitoris.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 May 2015. Web. 26 May 2015.

45. Wikipedia contributors. “Clitoris.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Jan 2016. Web. 1 Feb 2016.

46. Wikipedia contributors. “Cluster headache.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 6 Jan 2016. Web. 27 Jan 2016.

47. Wikipedia contributors. “Colloid.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 6 Jul 2016. Web. 20 Jul 2016.

48. Wikipedia contributors. “Color blindness.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Feb 2015. Web. 18 Feb 2015.

49. Wikipedia contributors. “Comedo.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 5 Jul 2015. Web. 7 Jul 2015.

50. Wikipedia contributors. “Comprehensive metabolic panel.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 14 Oct 2014. Web. 27 Feb 2015.

51. Wikipedia contributors. “Condom.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 29 Apr 2014. Web. 30 Apr 2014.

52. Wikipedia contributors. “Congenital heart defect.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 26 Aug 2014. Web. 2 Sep 2014.

53. Wikipedia contributors. “Contraceptive sponge.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 10 Apr 2014. Web. 30 Apr 2014.

54. Wikipedia contributors. “Cornea.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 May 2016. Web. 15 Jul 2016.

55. Wikipedia contributors. “Corneal abrasion.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 26 May 2016. Web. 1 Jul 2016.

56. Wikipedia contributors. “Coronary circulation.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Jun 2014. Web. 26 Jun 2014.

57. Wikipedia contributors. “Coronavirus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Feb 2015. Web. 24 Apr 2015.

58. Wikipedia contributors. “Coxiella burnetii.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Oct 2015. Web. 24 Nov 2015.

59. Wikipedia contributors. “Cramp.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 31 Jul 2015. Web. 6 Aug 2015.

60. Wikipedia contributors. “Cranial nerves.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 May 2015. Web. 28 May 2015.

61. Wikipedia contributors. “Cricothyrotomy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Aug 2015. Web. 22 Sep 2015.

62. Wikipedia contributors. “Crista.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Oct 2014. Web. 4 Nov 2015.

63. Wikipedia contributors. “Cross-sectional study.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 8 Nov 2015. Web. 27 Nov 2015.

64. Wikipedia contributors. “Crown (dentistry).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 15 May 2014. Web. 16 Jul 2014.

65. Wikipedia contributors. “Cytosol.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Dec 2015. Web. 8 Feb 2016.

66. <span>Wikipedia contributors. “Dacryoadenitis.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 10 Aug 2014. Web. 22 Sep 2014.</span>

67. Wikipedia contributors. “Deciduous teeth.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jul 2014. Web. 16 Jul 2014.

68. Wikipedia contributors. “Dengue fever.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Jan 2016. Web. 29 Jan 2016.

69. Wikipedia contributors. “Depression (mood).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 31 Mar 2015. Web. 3 Apr 2015.

70. Wikipedia contributors. “Dermabrasion.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Jun 2015. Web. 7 Jul 2015.

71. Wikipedia contributors. “Descending aorta.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Mar 2016. Web. 5 Oct 2016.

72. Wikipedia contributors. “Diaphragm (contraceptive).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 14 Apr 2014. Web. 30 Apr 2014.

73. Wikipedia contributors. “Digit (anatomy).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 15 May 2014. Web. 1 Jul 2014.

74. Wikipedia contributors. “Dissection (medical).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Apr 2016. Web. 6 Oct 2016.

75. Wikipedia contributors. “Down syndrome.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 17 Mar 2015. Web. 19 Mar 2015.

76. Wikipedia contributors. “Duchenne muscular dystrophy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 6 May 2015. Web. 8 May 2015.

77. Wikipedia contributors. “Dysarthria.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 5 Jul 2015. Web. 31 Aug 2015.

78. Wikipedia contributors. “Dysphoria.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 5 Mar 2015. Web. 19 Mar 2015.

79. Wikipedia contributors. “Dysthymia.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 28 Feb 2015. Web. 3 Apr 2015.

80. Wikipedia contributors. “Ebola virus disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Jun 2016. Web. 17 Jun 2016.

81. Wikipedia contributors. “Elastase.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Apr 2014. Web. 26 Jun 2014.

82. Wikipedia contributors. “Endemic (epidemiology).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Jun 2015. Web. 27 Nov 2015.

83. Wikipedia contributors. “Endocarditis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 17 Feb 2014. Web. 9 Jul 2014.

84. Wikipedia contributors. “Endocardium.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jun 2014. Web. 9 Jul 2014.

85. Wikipedia contributors. “End-of-life care.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 8 Jul 2016. Web. 7 Sep 2016.

86. Wikipedia contributors. “Endometriosis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 1 Jun 2015. Web. 8 Jun 2015.

87. Wikipedia contributors. “Endomysium.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 May 2015. Web. 13 Jul 2015.

88. Wikipedia contributors. “Endosteum.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Dec 2014. Web. 28 May 2015.

89. Wikipedia contributors. “EndoStim Electrical Stimulation Therapy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 10 Jun 2015. Web. 11 Jun 2015.

90. Wikipedia contributors. “Enteric nervous system.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Jun 2015. Web. 18 Sep 2015.

91. Wikipedia contributors. “Epiphyseal plate.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 26 May 2015. Web. 29 May 2015.

92. Wikipedia contributors. “Epithalamus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 26 Mar 2015. Web. 26 Mar 2015.

93. Wikipedia contributors. “Epstein-Barr virus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 Feb 2015. Web. 7 Feb 2015.

94. Wikipedia contributors. “Erysipelas.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Nov 2015. Web. 24 Nov 2015.

95. Wikipedia contributors. “Esophageal hiatus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 Oct 2016. Web. 21 Oct 2016.

96. Wikipedia contributors. “Eye.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 May 2014. Web. 23 May 2014.

97. Wikipedia contributors. “Eyelid.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 15 May 2014. Web. 23 May 2014.

98. Wikipedia contributors. “Fasciotomy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 28 May 2015. Web. 5 Jun 2015.

99. Wikipedia contributors. “Fetal alcohol syndrome.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Jan 2015. Web. 18 Feb 2015.

100. Wikipedia contributors. “Fibrillin.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 May 2014. Web. 21 July 2014.

101. Wikipedia contributors. “Fibromyalgia.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Apr 2015. Web. 24 Apr 2015.

102. Wikipedia contributors. “Fibrosis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 Apr 2014. Web. 19 Jun 2014.

103. Wikipedia contributors. “Fibula.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Apr 2015. Web. 29 Apr 2015.

104. Wikipedia contributors. “Flap (surgery).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Dec 2015. Web. 4 Feb 2016.

105. Wikipedia contributors. “Flexor retinaculum of the hand.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 22 May 2014. Web. 1 Jul 2014.

106. Wikipedia contributors. “Forest plot.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Nov 2015. Web. 18 Nov 2015.

107. Wikipedia contributors. “Fovea centralis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Jun 2014. Web. 2 Jul 2014.

108. Wikipedia contributors. “Fundal height.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 31 May 2016. Web. 21 Oct 2016.

109. Wikipedia contributors. “Fundus (eye).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 14 Jun 2016. Web. 21 Oct 2016.

110. Wikipedia contributors. “Gastric bypass surgery.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 27 Apr 2014. Web. 30 Apr 2014.

111. Wikipedia contributors. “Graft (surgery).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Nov 2015. Web. 4 Feb 2016.

112. Wikipedia contributors. “Granuloma.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Feb 2014. Web. 23 Jun 2014.

113. Wikipedia contributors. “Guillain-Barré syndrome.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Feb 2015. Web. 3 Mar 2015.

114. Wikipedia contributors. “Hair loss.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Apr 2015. Web. 1 Jun 2015.

115. Wikipedia contributors. “Hamate bone.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Jun 2014. Web. 1 Jul 2014.

116. Wikipedia contributors. “Head lice infestation.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 Dec 2015. Web. 29 Jan 2016.

117. <span>Wikipedia contributors. “Head louse.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 24 Dec 2015. Web. 29 Jan 2016.</span>

118. Wikipedia contributors. “Hemagglutinin (influenza).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 1 Sep 2015. Web. 18 Nov 2015.

119. Wikipedia contributors. “Hemangioma.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 May 2015. Web. 4 Jun 2015.

120. Wikipedia contributors. “Hemangioma.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 May 2015. Web. 1 Jul 2015.

121. Wikipedia contributors. “Hemorrhoid.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Mar 2015. Web. 20 Apr 2015.

122. Wikipedia contributors. “Herpes zoster.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jun 2014. Web. 4 Jun 2014.

123. <span>Wikipedia contributors. “Herpes zoster.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 3 Jun 2014. Web. 18 Mar 2015.</span>

124. Wikipedia contributors. “Human mouth.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 1 Nov 2016. Web. 18 Nov 2016.

125. Wikipedia contributors. “Human musculoskeletal system.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Jan 2016. Web. 29 Jan 2016.

126. <span>Wikipedia contributors. “Human serum albumin.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 26 Feb 2015. Web. 26 Feb 2015.</span>

127. Wikipedia contributors. “Hypermobility (joints).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Apr 2015. Web. 24 Apr 2015.

128. Wikipedia contributors. “Hyperpigmentation.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Feb 2015. Web. 5 Jun 2015.

129. Wikipedia contributors. “Hypoalbuminemia.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 26 Feb 2015. Web. 27 Feb 2015.

130. Wikipedia contributors. “Hypopigmentation.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 30 Aug 2014. Web. 5 Jun 2015.

131. Wikipedia contributors. “Inborn error of metabolism.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 Mar 2015. Web. 16 Mar 2015.

132. Wikipedia contributors. “Infection.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 Jun 2014. Web. 24 Jun 2014.

133. Wikipedia contributors. “Influenza.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Nov 2015. Web. 18 Nov 2015.

134. Wikipedia contributors. “Inguinal canal.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 May 2015. Web. 12 Jun 2015.

135. Wikipedia contributors. “Internal jugular vein.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 10 May 2015. Web. 22 Sep 2015.

136. Wikipedia contributors. “Interstitial.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Jun 2014. Web. 19 Jun 2014.

137. Wikipedia contributors. “Interstitial cystitis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Aug 2015. Web. 18 Sep 2015.

138. Wikipedia contributors. “Interventricular septum.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 Sept 2013. Web. 6 May 2014.

139. Wikipedia contributors. “Intracranial aneurysm.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Aug 2016. Web. 5 Oct 2016.

140. Wikipedia contributors. “Intracytoplasmic sperm injection.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 30 Mar 2015. Web. 22 May 2015.

141. Wikipedia contributors. “Intravitreal administration.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 8 Dec 2013. Web. 21 Apr 2015.

142. Wikipedia contributors. “Ischium.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 Nov 2015. Web. 25 Nov 2015.

143. Wikipedia contributors. “Itch.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 March 2016. Web. 29 Mar 2016.

144. Wikipedia contributors. “Kayser-Fleischer ring.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 22 Oct 2014. Web. 28 Apr 2016.

145. Wikipedia contributors. “Keratin.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 Feb 2015. Web. 3 Apr 2015.

146. Wikipedia contributors. “Keratinocyte.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 Aug 2016. Web. 2 Nov 2016.

147. Wikipedia contributors. “Keratoconus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 5 Jan 2016. Web. 5 Jan 2016.

148. Wikipedia contributors. “Kidney disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Nov 2015. Web. 10 Dec 2015.

149. Wikipedia contributors. “Knee.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Mar 2015. Web. 3 Mar 2015.

150. Wikipedia contributors. “Knee pain.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Feb 2015. Web. 3 Mar 2015.

151. Wikipedia contributors. “Labia.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 May 2015. Web. 26 May 2015.

152. Wikipedia contributors. “Lesion.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 Feb 2014. Web. 16 May 2014.

153. Wikipedia contributors. “Lichen planus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 1 Sep 2015. Web. 20 Nov 2015.

154. Wikipedia contributors. “Lichen sclerosus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Jun 2015. Web. 6 Jul 2015.

155. <span>Wikipedia contributors. “Limb-girdle muscular dystrophy.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 5 Feb 2015. Web. 8 May 2015.</span>

156. Wikipedia contributors. “Lingual papilla.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 30 Nov 2015. Web. 1 Dec 2015.

157. Wikipedia contributors. “Lingual tonsils.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 9 Nov 2015. Web. 23 Nov 2015.

158. Wikipedia contributors. “Lipid storage disorder.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 Apr 2016. Web. 7 Jun 2016.

159. Wikipedia contributors. “Loop diuretic.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Sep 2015. Web. 17 Sep 2015.

160. Wikipedia contributors. “Lunate bone.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Mar 2014. Web. 1 Jul 2014.

161. Wikipedia contributors. “Lung transplantation.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 10 May 2016. Web. 19 Jul 2016.

162. Wikipedia contributors. “Lymphatic system.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 28 Oct 2015. Web. 23 Nov 2015.

163. <span>Wikipedia contributors. “Major depressive disorder.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 25 Mar 2015. Web. 3 Apr 2015.</span>

164. Wikipedia contributors. “Male infertility.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 27 Apr 2015. Web. 26 May 2015.

165. Wikipedia contributors. “Massage.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 29 Jan 2016. Web. 29 Jan 2016.

166. Wikipedia contributors. “Medial.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Apr 2014. Web. 10 Jun 2014.

167. Wikipedia contributors. “Median nerve.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 30 Apr 2014. Web. 1 Jul 2014.

168. Wikipedia contributors. “Medulla oblongata.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Oct 2014. Web. 3 Nov 2014.

169. Wikipedia contributors. “Metacarpophalangeal joint.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 Mar 2014. Web. 1 Jul 2014.

170. Wikipedia contributors. “Microalbuminuria.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 29 Apr 2014. Web. 8 Jul 2014.

171. Wikipedia contributors. “Microvessel.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Mar 2016. Web. 1 Jul 2016.

172. Wikipedia contributors. “Middle East respiratory syndrome.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Feb 2015. Web. 24 Apr 2015.

173. Wikipedia contributors. “Migraine.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Jan 2016. Web. 27 Jan 2016.

174. Wikipedia contributors. “Muscle.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 May 2014. Web. 27 May 2014.

175. Wikipedia contributors. “Muscle fascicle.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 9 Jun 2015. Web. 13 Jul 2015.

176. Wikipedia contributors. “Muscular dystrophy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 29 Apr 2015. Web. 4 May 2015.

177. Wikipedia contributors. “Mycobacterium.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 May 2016. Web. 3 Oct 2016.

178. Wikipedia contributors. “Myoglobin.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Mar 2015. Web. 17 Mar 2015.

179. Wikipedia contributors. “Myotonic dystrophy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Apr 2015. Web. 8 May 2015.

180. Wikipedia contributors. “Nail (anatomy).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Apr 2015. Web. 3 Apr 2015.

181. Wikipedia contributors. “Nasal septum.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Feb 2014. Web. 6 May 2014.

182. Wikipedia contributors. “Neck pain.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 26 Jan 2016. Web. 29 Jan 2016.

183. Wikipedia contributors. “Neuraminidase inhibitor.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Nov 2015. Web. 19 Nov 2015.

184. Wikipedia contributors. “Neurodevelopmental disorder.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Feb 2015. Web. 20 Feb 2015.

185. Wikipedia contributors. “Nissen fundoplication.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Mar 2015. Web. 9 Jun 2015.

186. Wikipedia contributors. “Nissen fundoplication.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Jul 2016. Web. 21 Oct 2016.

187. Wikipedia contributors. “Nontuberculous mycobacteria.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Jun 2016. Web. 1 Jul 2016.

188. Wikipedia contributors. “Norovirus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 Apr 2015. Web. 20 Apr 2015.

189. Wikipedia contributors. “Nutrient canal.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jun 2015. Web. 10 Jun 2015.

190. Wikipedia contributors. “Occlusion (dentistry).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 17 Jul 2014. Web. 2 Jul 2015.

191. Wikipedia contributors. “Odor.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Jul 2015. Web. 16 Jul 2015.

192. Wikipedia contributors. “Olfactory nerve.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 6 Feb 2015. Web. 28 May 2015.

193. Wikipedia contributors. “Olfactory tract.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Apr 2015. Web. 28 May 2015.

194. Wikipedia contributors. “Ora serrata.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Oct 2013. Web. 23 May 2014.

195. Wikipedia contributors. “Osmotic diuretic.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 30 Aug 2015. Web. 21 Sep 2015.

196. Wikipedia contributors. “Osteoblast.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 9 Apr 2015. Web. 4 Jun 2015.

197. Wikipedia contributors. “Otitis media.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 9 Jul 2015. Web. 24 Jul 2015.

198. Wikipedia contributors. “Otitis media.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 25 Aug 2015. Web. 27 Aug 2015.

199. Wikipedia contributors. “Otolith.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 Feb 2015. Web. 23 Apr 2015.

200. Wikipedia contributors. “Ottawa ankle rules.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Aug 2014. Web. 29 Apr 2015.

201. <span>Wikipedia contributors. “Oval window.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 26 Dec 2014. Web. 23 Apr 2015.</span>

202. Wikipedia contributors. “Ovarian cyst.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 28 Feb 2015. Web. 23 Mar 2015.

203. Wikipedia contributors. “Ovarian follicle.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 17 Jan 2015. Web. 19 Mar 2015.

204. Wikipedia contributors. “Palm.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 27 Feb 2014. Web. 1 Jul 2014.

205. Wikipedia contributors. “Panic disorder.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 7 Apr 2015. Web. 15 Apr 2015.

206. Wikipedia contributors. “Parasitic disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Dec 2014. Web. 2 Mar 2015.

207. Wikipedia contributors. “Pars plana.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 22 Mar 2014. Web. 23 May 2014.

208. Wikipedia contributors. “Pelvic pain.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 29 Sep 2014. Web. 8 Oct 2014.

209. Wikipedia contributors. “Pemphigoid.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Jun 2015. Web. 24 Jun 2015.

210. Wikipedia contributors. “Pericardium.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 May 2014. Web. 9 Jul 2014.

211. Wikipedia contributors. “Perimysium.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 May 2015. Web. 13 Jul 2015.

212. Wikipedia contributors. “Periodontium.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 20 January 2014. Web. 16 July 2014.

213. Wikipedia contributors. “Peripheral vision.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 May 2014. Web. 2 Jul 2014.

214. Wikipedia contributors. “Phalanx bone.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 Jun 2014. Web. 1 Jul 2014.

215. Wikipedia contributors. “Phenylketonuria.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Mar 2015. Web. 16 Mar 2015.

216. <span>Wikipedia contributors. “Photoreceptor cell.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 28 Jan 2015. Web. 18 Feb 2015.</span>

217. Wikipedia contributors. “Photorejuvenation.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jul 2015. Web. 7 Jul 2015.

218. Wikipedia contributors. “Pilates.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 22 May 2015. Web. 16 Jul 2015.

219. <span>Wikipedia contributors. “Pisiform bone.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 7 Jun 2014. Web. 1 Jul 2014.</span>

220. Wikipedia contributors. “Plague (disease).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Feb 2016. Web. 3 Feb 2016.

221. <span>Wikipedia contributors. “Pleural disease.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 23 Mar 2014. Web. 26 Aug 2014.</span>

222. <span>Wikipedia contributors. “Polyneuropathy.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 17 Jan 2015. Web. 3 Mar 2015.</span>

223. Wikipedia contributors. “Post-concussion syndrome.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Apr 2015. Web. 24 Apr 2015.

224. Wikipedia contributors. “Posterior cruciate ligament.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jun 2014. Web. 10 Jun 2014.

225. Wikipedia contributors. “Potassium-sparing diuretic.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Sep 2014. Web. 21 Sep 2015.

226. Wikipedia contributors. “Premenstrual dysphoric disorder.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 15 Dec 2014. Web. 19 Mar 2015.

227. Wikipedia contributors. “Premenstrual syndrome.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Mar 2015. Web. 19 Mar 2015.

228. Wikipedia contributors. “Prepuce.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 14 Oct 2014. Web. 1 Feb 2016.

229. Wikipedia contributors. “Pressure ulcer.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Nov 2015. Web. 25 Nov 2015.

230. Wikipedia contributors. “Prevalence.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 7 Nov 2016. Web. 22 Nov 2016.

231. Wikipedia contributors. “Progressive disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 30 Jun 2016. Web. 7 Sep 2016.

232. Wikipedia contributors. “Proprioception.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Apr 2015. Web. 29 Apr 2015.

233. Wikipedia contributors. “Psychotic depression.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Apr 2015. Web. 21 Apr 2015.

234. Wikipedia contributors. “Pulp (tooth).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 June 2014. Web. 16 July 2014.

235. Wikipedia contributors. “Punctal plug.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Apr 2014. Web. 22 Sep 2014.

236. Wikipedia contributors. “Raynaud’s phenomenon.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 27 Jul 2015. Web. 8 Sep 2015.

237. Wikipedia contributors. “Recurrent laryngeal nerve.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 28 Aug 2015. Web. 22 Sep 2015.

238. Wikipedia contributors. “Renal biopsy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 May 2014. Web. 11 July 2014.

239. Wikipedia contributors. “Renin inhibitor.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 27 Aug 2015. Web. 18 Sep 2015.

240. Wikipedia contributors. “Retinitis pigmentosa.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 17 Jan 2015. Web. 18 Feb 2015.

241. Wikipedia contributors. “Retinopathy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 5 Sep 2016. Web. 3 Oct 2016.

242. Wikipedia contributors. “Retinopathy of Prematurity.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Aug 2016. Web. 3 Oct 2016.

243. Wikipedia contributors. “Retraction.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Jan 2016. Web. 1 Feb 2016.

244. Wikipedia contributors. “Rhabdomyolysis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Feb 2015. Web. 17 Mar 2015.

245. Wikipedia contributors. “Rheumatism.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Oct 2016. Web. 26 Oct 2016.

246. Wikipedia contributors. “Rhinophyma.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 1 Apr 2015. Web. 21 Apr 2015.

247. Wikipedia contributors. “Rotator cuff tear.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 May 2014. Web. 27 May 2014.

248. Wikipedia contributors. “Route of administration.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 6 Sep 2015. Web. 19 Nov 2015.

249. Wikipedia contributors. “Saccule.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 7 Apr 2015. Web. 23 Apr 2015.

250. <span>Wikipedia contributors. “Salivary gland.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 25 Feb 2015. Web. 14 Dec 2014.</span>

251. Wikipedia contributors. “Salmonellosis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 Nov 2015. Web. 25 Nov 2015.

252. Wikipedia contributors. “Salter-Harris fracture.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Jun 2014. Web. 6 Jul 2016.

253. Wikipedia contributors. “Scaphoid bone.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Mar 2014. Web. 1 Jul 2014.

254. Wikipedia contributors. “Schizophrenia.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 31 Mar 2015. Web. 3 Apr 2015.

255. <span>Wikipedia contributors. “Schwann cell.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 10 Sep 2015. Web. </span><span>15 Oct 2015.</span>

256. Wikipedia contributors. “Semen analysis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 14 May 2015. Web. 22 May 2015.

257. Wikipedia contributors. “Semicircular canal.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Feb 2015. Web. 23 Apr 2015.

258. Wikipedia contributors. “Serous fluid.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Apr 2014. Web. 9 Jul 2014.

259. Wikipedia contributors. “Sickle-cell disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 15 Mar 2014. Web. 21 Mar 2014.

260. Wikipedia contributors. “Sinusitis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Dec 2014. Web. 4 Mar 2015.

261. Wikipedia contributors. “Skin grafting.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Dec 2015. Web. 4 Feb 2016.

262. Wikipedia contributors. “Sleep deprivation.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Aug 2014. Web. 25 Aug 2014.

263. Wikipedia contributors. “Sleeve gastrectomy.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 23 Apr 2014. Web. 30 Apr 2014.

264. Wikipedia contributors. “Soma (biology).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jun 2014. Web. 13 Nov 2014.

265. Wikipedia contributors. “Spermicide.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 22 Apr 2014. Web. 30 Apr 2014.

266. Wikipedia contributors. “Spinal cord stimulator.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Jul 2015. Web. 16 Jul 2015.

267. Wikipedia contributors. “Spinal nerve.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 Feb 2015. Web. 24 Feb 2015.

268. Wikipedia contributors. “Sprained ankle.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 22 Mar 2015. Web. 29 Apr 2015.

269. Wikipedia contributors. “Squamous-cell carcinoma.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Mar 2015. Web. 19 Mar 2015.

270. Wikipedia contributors. “Staphylococcus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 4 Nov 2015. Web. 25 Nov 2015.

271. Wikipedia contributors. “Stargardt disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 21 Mar 2015. Web. 21 Apr 2015.

272. Wikipedia contributors. “Stenosis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 8 Sep 2016. Web. 25 Oct 2016.

273. Wikipedia contributors. “Streptococcal pharyngitis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Mar 2015. Web. 17 Mar 2015.

274. Wikipedia contributors. “Stress fracture.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 31 Mar 2016. Web. 19 May 2016.

275. Wikipedia contributors. “Stridor.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 8 Dec 2014. Web. 21 May 2015.

276. Wikipedia contributors. “Subarachnoid hemorrhage.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 5 Oct 2016. Web. 6 Oct 2016.

277. Wikipedia contributors. “Subcutaneous emphysema.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 1 Jun 2014. Web. 6 Jun 2014.

278. Wikipedia contributors. “Sunburn.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 19 Aug 2015. Web. 18 Sep 2015.

279. Wikipedia contributors. “Superficial inguinal ring.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Apr 2015. Web. 15 Jun 2015.

280. Wikipedia contributors. “Synovial bursa.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 17 Feb 2014. Web. 27 May 2014.

281. <span>Wikipedia contributors. “Synovial joint.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 11 Jan 2015. Web. 1 Jul 2015.</span>

282. Wikipedia contributors. “Systematic review.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Nov 2015. Web. 19 Nov 2015.

283. Wikipedia contributors. “Systemic circulation.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Jun 2014. Web. 26 Jun 2014.

284. Wikipedia contributors. “Taste (disambiguation).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 14 Mar 2013. Web. 1 Dec 2015.

285. Wikipedia contributors. “Taste bud.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 Sep 2015. Web. 1 Dec 2015.

286. Wikipedia contributors. “Taste bud.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 12 Oct 2016. Web. 25 Oct 2016.

287. Wikipedia contributors. “Tay-Sachs disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 5 Mar 2015. Web. 16 Mar 2015.

288. Wikipedia contributors. “Temporal bone.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 30 Jun 2015. Web. 1 Jul 2015.

289. <span>Wikipedia contributors. “Temporomandibular joint.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 30 May 2015. Web. 1 Jul 2015.</span>

290. <span>Wikipedia contributors. “Temporomandibular joint dysfunction.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 9 Jun 2015. Web. 1 Jul 2015.</span>

291. Wikipedia contributors. “Tendon sheath.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 Mar 2013. Web. 1 Jul 2014.

292. Wikipedia contributors. “Tension headache.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 13 Jan 2016. Web. 27 Jan 2016.

293. Wikipedia contributors. “Testicular torsion.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Apr 2015. Web. 27 May 2015.

294. Wikipedia contributors. “Thoracic aorta.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 3 Jan 2016. Web. 5 Oct 2016.

295. Wikipedia contributors. “Thoracic aortic aneurysm.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 4 Feb 2015. Web. 5 Oct 2016.

296. Wikipedia contributors. “Thoracic vertebrae.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 22 Feb 2015. Web. 23 Feb 2015.

297. Wikipedia contributors. “Thyroid cartilage.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 24 May 2015. Web. 22 Sep 2015.

298. Wikipedia contributors. “Thyroid disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 15 May 2015. Web. 22 Dec 2015.

299. Wikipedia contributors. “Thyroid nodule.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Nov 2015. Web. 22 Dec 2015.

300. Wikipedia contributors. “Tinea versicolor.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 27 May 2015. Web. 5 Jun 2015.

301. Wikipedia contributors. “Tongue cleaner.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 7 Apr 2015. Web. 16 Jul 2015.

302. Wikipedia contributors. “Trabecular meshwork.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 2 Mar 2014. Web. 23 May 2014.

303. Wikipedia contributors. “Tracheobronchial lymph nodes.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 18 May 2014. Web. 22 Sep 2015.

304. Wikipedia contributors. “Transmission (medicine).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 30 May 2014. Web. 24 Jun 2014.

305. Wikipedia contributors. “Trapezium (bone).” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Mar 2014. Web. 1 Jul 2014.

306. Wikipedia contributors. “Trapezoid bone.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Mar 2014. Web. 1 Jul 2014.

307. Wikipedia contributors. “Triquetral bone.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Mar 2014. Web. 1 Jul 2014.

308. Wikipedia contributors. “Tympanic cavity.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Jan 2015. Web. 23 Apr 2015.

309. Wikipedia contributors. “Urea.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 8 Nov 2016. Web. 17 Nov 2016.

310. Wikipedia contributors. “Urethritis.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 15 Aug 2015. Web. 23 Sep 2015.

311. Wikipedia contributors. “Uterine fibroid.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 11 Jan 2015. Web. 6 Mar 2015.

312. <span>Wikipedia contributors. “Varicella zoster virus.” </span>*Wikipedia, The Free Encyclopedia*<span>. Wikipedia, The Free Encyclopedia, 12 Apr 2015. Web. 15 Apr 2015.</span>

313. Wikipedia contributors. “Varicocele.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 16 Apr 2014. Web. 21 May 2015.

314. Wikipedia contributors. “Varicose veins.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 27 Sep 2016. Web. 2 Nov 2016.

315. Wikipedia contributors. "Venous thrombosis." *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 27 Mar 2014. Web. 3 Apr 2014.

316. Wikipedia contributors. “Vesicoureteral reflux.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 28 Nov 2015. Web. 18 Dec 2015.

317. Wikipedia contributors. “Whipple’s disease.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 6 Sep 2015. Web. 9 Oct 2015.

318. Wikipedia contributors. “Zika virus.” *Wikipedia, The Free Encyclopedia*. Wikipedia, The Free Encyclopedia, 28 Jan 2016. Web. 28 Jan 2016.

[Return to top](#top)

Wiktionary
----------

1.  Wiktionary contributors. “Acne.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 22 Apr 2015. Web. 7 Jul 2015.

2.  Wiktionary contributors. “Adrenergic.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 13 Aug 2015. Web. 18 Sep 2015.

3.  Wiktionary contributors. “Arm.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 25 Jun 2014. Web. 1 Jul 2014.

4.  Wiktionary contributors. “Astigmatism.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 24 May 2014. Web. 26 Jun 2014.

5.  Wiktionary contributors. “Audible.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 6 Jun 2014. Web. 8 Jul 2014.

6.  Wiktionary contributors. “Bipolar disorder.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 23 Feb 2015. Web. 4 Mar 2015.

7.  Wiktionary contributors. “Broca’s area.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 23 Oct 2013. Web. 2 Sep 2015.

8.  <span>Wiktionary contributors. “Bulbous.” </span>*Wiktionary, The Free Dictionary*<span>. Wiktionary, The Free Dictionary, 15 Dec 2014. Web. 20 Feb 2015.</span>

9.  Wiktionary contributors. “Carpus.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 16 Jun 2014. Web. 1 Jul 2014.

10. Wiktionary contributors. “Cementum.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 21 Jun 2014. Web. 16 Jul 2014.

11. Wiktionary contributors. “Centriole.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 7 Jul 2014. Web. 16 Nov 2016.

12. Wiktionary contributors. “Chamber.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 7 Apr 2014. Web. 23 May 2014.

13. Wiktionary contributors. “Condyle.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 10 Apr 2015. Web. 1 Jul 2015.

14. Wiktionary contributors. “Cone cell.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 4 Aug 2014. Web. 18 Feb 2015.

15. Wiktionary contributors. “Congenital.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 7 Apr 2014. Web. 10 Jul 2014.

16. Wiktionary contributors. “Corpus callosum.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 15 Dec 2014. Web. 23 Mar 2015.

17. Wiktionary contributors. “Denominator.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 8 Nov 2016. Web. 21 Nov 2016.

18. Wiktionary contributors. “Dermatomyositis.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 11 Dec 2014. Web. 24 Aug 2015.

19. Wiktionary contributors. “Digit.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 24 May 2014. Web. 1 Jul 2014.

20. Wiktionary contributors. “Disaccharide.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 24 May 2014. Web. 21 Jul 2014.

21. Wiktionary contributors. “Diuretic.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 9 Jul 2015. Web. 23 Sep 2015.

22. Wiktionary contributors. “Effusion.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 16 December 2014. Web. 27 Aug 2015.

23. Wiktionary contributors. “Enamel.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 9 Jul 2014. Web. 15 Jul 2014.

24. Wiktionary contributors. “Endosseous.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 27 Mar 2012. Web. 1 Jul 2015.

25. Wiktionary contributors. “Epidemic.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 21 Oct 2015. Web. 27 Nov 2015.

26. Wiktionary contributors. “Erysipelas.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 23 Oct 2015. Web. 24 Nov 2015.

27. Wiktionary contributors. “Exogenous.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 20 Jun 2013. Web. 23 Jul 2014.

28. Wiktionary contributors. “Flexor.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 20 May 2014. Web. 1 Jul 2014.

29. Wiktionary contributors. “Foramen.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 12 Feb 2015. Web. 3 Jun 2015.

30. Wiktionary contributors. “Forearm.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 21 Jun 2014. Web. 1 Jul 2014.

31. <span>Wiktionary contributors. “Genital wart.” </span>*Wiktionary, The Free Dictionary*<span>. Wiktionary, The Free Dictionary, 11 Dec 2014. Web. 28 Sep 2015.</span>

32. Wiktionary contributors. “Halitosis.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 11 Apr 2015. Web. 16 Jul 2015.

33. Wiktionary contributors. “Hand.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 19 Jun 2014. Web. 1 Jul 2014.

34. Wiktionary contributors. “Heel.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 17 Nov 2015. Web. 25 Nov 2015.

35. Wiktionary contributors. “Heimlich maneuver.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 24 Mar 2015. Web. 22 Sep 2015.

36. Wiktionary contributors. “Hyperopia.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 23 May 2014. Web. 25 Jun 2014.

37. Wiktionary contributors. “Inhibitor.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 24 Jul 2015. Web. 18 Sep 2015.

38. Wiktionary contributors. “Insoluble.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 17 Jun 2016. Web. 20 Jul 2016.

39. Wiktionary contributors. “Intraoral.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 27 May 2014. Web. 16 Jul 2015.

40. Wiktionary contributors. “Jugular Vein.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 10 Jan 2014. Web. 23 Jun 2014.

41. Wiktionary contributors. “Lateral.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 24 May 2014. Web. 10 Jun 2014.

42. Wiktionary contributors. “Limbus.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 16 Apr 2014. Web. 23 May 2014.

43. Wiktionary contributors. “Localized.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 11 Sep 2016. Web. 5 Oct 2016.

44. Wiktionary contributors. “Loop of Henle.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 19 Jun 2013. Web. 17 Sep 2015.

45. Wiktionary contributors. “Macromolecule.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 16 Oct 2015. Web. 18 Nov 2015.

46. Wiktionary contributors. “Maltose.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 7 Apr 2014. Web. 21 Jul 2014.

47. Wiktionary contributors. “Medial.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 24 May 2014. Web. 10 Jun 2014.

48. Wiktionary contributors. “Medulla.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 11 Dec 2014. Web. 10 Jul 2015.

49. Wiktionary contributors. “Mesentery.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 9 Nov 2015. Web. 18 Nov 2015.

50. Wiktionary contributors. “Mononucleosis.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 26 Feb 2015. Web. 23 May 2014.

51. Wiktionary contributors. “Mumps.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 18 Jan 2016. Web. 18 Feb 2016.

52. Wiktionary contributors. “Mycobacterium.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 22 Jan 2016. Web. 3 Oct 2016.

53. Wiktionary contributors. “Myocardium.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 17 Apr 2014. Web. 26 Jun 2014.

54. Wiktionary contributors. “Nerve impulse.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 18 Sep 2014. Web. 20 Feb 2015.

55. Wiktionary contributors. “Nervous system.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 26 Jul 2015. Web. 18 Sep 2015.

56. Wiktionary contributors. “Numerator.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 8 Nov 2016. Web. 21 Nov 2016.

57. Wiktionary contributors. “Odor.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 6 May 2015. Web. 16 Jul 2015.

58. Wiktionary contributors. “Palatine tonsil.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 2 Sep 2015. Web. 23 Nov 2015.

59. Wiktionary contributors. “Pemphigus.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 7 Mar 2015. Web. 24 Jun 2015.

60. Wiktionary contributors. “Perihilar.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 26 May 2014. Web. 1 Oct 2015.

61. Wiktionary contributors. “Perinatal.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 20 Jan 2014. Web. 24 Jun 2014.

62. Wiktionary contributors. “Photosensitivity.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 26 Aug 2013. Web. 21 Apr 2015.

63. Wiktionary contributors. “Pilosebaceous unit.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 22 Mar 2015. Web. 6 Jul 2015.

64. Wiktionary contributors. “Pore.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 8 Mar 2015. Web. 7 Jul 2015.

65. Wiktionary contributors. “Premenstrual.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 17 Dec 2014. Web. 19 Mar 2015.

66. Wiktionary contributors. “Protease.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 25 May 2014. Web. 16 Jun 2014.

67. Wiktionary contributors. “Pulmonary emphysema.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 15 Dec 2013. Web. 22 Aug 2014.

68. Wiktionary contributors. “Pustule.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 26 Feb 2015. Web. 7 Jul 2015.

69. Wiktionary contributors. “Retract.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 18 Jan 2016. Web. 1 Feb 2016.

70. <span>Wiktionary contributors. “Rod.” </span>*Wiktionary, The Free Dictionary*<span>. Wiktionary, The Free Dictionary, 1 Feb 2015. Web. 18 Feb 2015.</span>

71. Wiktionary contributors. “Root.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 7 Jul 2014. Web. 16 Jul 2014.

72. Wiktionary contributors. “Sac.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 22 Apr 2014. Web. 23 May 2014.

73. Wiktionary contributors. “Salmonella.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 17 Oct 2015. Web. 24 Nov 2015.

74. Wiktionary contributors. “Sclerosis.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 28 May 2014. Web. 28 Jul 2014.

75. Wiktionary contributors. “Somatic nervous system.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 15 Feb 2015. Web. 18 Sep 2015.

76. Wiktionary contributors. “Stethoscope.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 23 May 2014. Web. 8 Jul 2014.

77. Wiktionary contributors. “Striated muscle.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 6 May 2014. Web. 13 Jul 2015.

78. Wiktionary contributors. “Suspension.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 14 Jul 2016. Web. 20 Jul 2016.

79. Wiktionary contributors. “Sweat.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 25 May 2014. Web. 5 Jun 2014.

80. Wiktionary contributors. “Temporomandibular joint.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 23 Oct 2014. Web. 1 Jul 2015.

81. Wiktionary contributors. “Thumb.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 6 Jun 2014. Web. 1 Jul 2014.

82. Wiktionary contributors. “Tics.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 18 Jan 2016. Web. 19 Feb 2016.

83. Wiktionary contributors. “Tympanic.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 17 Dec 2014. Web. 23 Apr 2015.

84. Wiktionary contributors. “Ulna.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 16 Jun 2014. Web. 1 Jul 2014.

85. Wiktionary contributors. “Vagus nerve.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 22 Mar 2015. Web. 8 Jul 2015.

86. Wiktionary contributors. “Vasculature.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 8 Oct 2013. Web. 23 May 2014.

87. Wiktionary contributors. “Whiplash.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 8 Dec 2015. Web. 29 Jan 2016.

88. Wiktionary contributors. “Wrist.” *Wiktionary, The Free Dictionary*. Wiktionary, The Free Dictionary, 6 Jun 2014. Web. 1 Jul 2014.

[Return to top](#top)


