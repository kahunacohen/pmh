Featured review
---------------

![Image of two women at the supermarket.](/core/assets/pmh/images/PMH_saycheese.png)
When the body gets its energy mostly from fats instead of carbohydrates, it produces substances called ketones. Although it is not clear why, a ketogene-producing diet (“ketogenic”) can reduce the number of seizures in some people with epilepsy.

The Atkins diet is a modified ketogenic diet. With Atkins, people eat twice as much fat as carbs. A full ketogenic diet is more severe: the ratio is four times as much fat. That is very hard to stick with over time. Close supervision is needed, because radical diets can be harmful. Stomach and gut problems are common, and there may be a chance of long-term adverse cardiovascular effects.

According to a review from the Cochrane Collaboration, there have now been some trials of ketogenic and modified ketogenic diets in children and young people with epilepsy. In all the trials, nearly 40% of the children and young people in the diet group had half or fewer seizures than they had before—including in the less radical diet trials.

Adverse effects like constipation were common (around 30%), but the less radical diets were easier to tolerate. The researchers found that only about 10% were still on the diets after a few years.

Research into modified diets for epilepsy is continuing.

You can read the Cochrane review summary [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011258/%20).

The full Cochrane review is on The Cochrane Library [here](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD001903.pub2/abstract%20).

Citation: Levy RG, Cooper PN, Giri P. Ketogenic diet and other dietary treatments for epilepsy. Cochrane Database of Systematic Reviews 2012, Issue 3. Art. No.: CD001903. DOI: 10.1002/14651858.CD001903.pub2. [Link to Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD001903.pub2/abstract)


