Featured review
---------------

![Girl with sinus pain.](/core/assets/pmh/images/PMH_sinus.jpg)
Nasal polyps are small growths inside the nose. They do not hurt and they are not cancerous. But they can block your nose and impair your sense of smell.

Around 2% to 4% of adults may have chronic sinusitis with nasal polyps. Chronic sinusitis is a stuffy nose and sinus problems that go on for 3 or more months.

Common treatments for nasal polyps are nasal sprays or drops with corticosteroids. Reviewers from the Cochrane Collaboration in Australia looked for trials testing the effects of corticosteroids used in the nose.

The reviewers found 40 trials, mostly of nasal sprays. Adverse effects were minor, and they could improve symptoms for many people.

You can read a summary of the results of this Cochrane review [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0050325/).

You can read about chronic sinusitis [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0041934/%20).

Citation: Kalish L, Snidvongs K, Sivasubramaniam R, Cope D, Harvey RJ. Topical steroids for nasal polyps. *Cochrane Database of Systematic Reviews* 2012, Issue 12. Art. No.: CD006549. DOI: 10.1002/14651858.CD006549.pub2. [Link to Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD006549.pub2/full).


