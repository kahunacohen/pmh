Featured review
---------------

![Photo of pain pills](/projects/pubmedhealth/featuredreviews/img/86533628_c.png)
Ear wax build-up is a common problem: what are the safe and effective options for clearing it from ears? The National Health Service's Health Technology Assessment Programme commissioned a review to find some answers.

See a summary of the results [for consumers](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0010389/) by IQWiG, the Institute for Quality and Efficiency in Health Care.

Or read the review's [executive summary](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0014924/).

[Linkout](http://www.hta.ac.uk/fullmono/mon1428.pdf) to the free full text in PDF.

Citation: Clegg AJ, Loveman E, Gospodarevskaya E, Harris P, Bird A, Bryant J, Scott DA, Davidson P, Little P, Coppin R. The safety and effectiveness of different methods of earwax removal: a systematic review and economic evauation [executive summary]. Health Technol Assess [Internet]. 2010 [cited 2011 Sept 15];14(28): ii-iv. Available from: <http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0014924/>

PubMed: <span class="Apple-style-span" style="white-space: nowrap; ">[20546687](http://www.ncbi.nlm.nih.gov/pubmed/20546687)</span>

*Interested in reviews on ear wax?* There is also a [Cochrane review](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012702/).


