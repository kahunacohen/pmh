Featured review
---------------

![Image of woman for featured review.](/core/assets/pmh/images/PMH_greywoman.png)
Treatment given after surgery for breast cancer, to lower the risk that the cancer will come back, is called adjuvant therapy. One of the options is chemotherapy. Chemotherapy or “chemo” is treatment with drugs that kill cancer cells.

Adjuvant chemotherapy for early breast cancer may involve various combinations of drugs (called polychemotherapy). A major systematic review from the Early Breast Cancer Trialists’ Collaborative Group compared these different combinations, by going back to the individual records of patients in the trials.

These trial results were analyzed in a complex study called an individual patient data (IPD) meta-analysis, to look for new insights from this far more detailed dataset. They were also able to address additional questions not covered in their earlier publications on polychemotherapy for early breast cancer.

The review, along with the critical summary of this review in the Database of Reviews of Effects (DARE) is [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0033107/%20).

Implications of differences between options for someone with breast cancer depend on their individual risks. You can read general information about early breast cancer and its treatment in information from the National Cancer Institute [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0032825/#CDR0000062955__185%20).

Citation: Early Breast Cancer Trialists' Collaborative Group (EBCTCG). Comparisons between different polychemotherapy regimens for early breast cancer: meta-analyses of long-term outcome among 100 000 women in 123 randomised trials. *Lancet*. 6 December 2011;6736(11)61625-5. Epub 2011 Dec 5. Cited in: PubMed. [PMID 22152853](http://www.ncbi.nlm.nih.gov/pubmed/22152853).


