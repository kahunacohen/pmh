Featured review
---------------

![Image of teens for featured review.](/core/assets/pmh/images/PMH_teens.png)
A systematic review by researchers at the University of London has confirmed that using alcohol self-help websites and computer programs can reduce alcohol use.

On average, people using the websites in trials drank several alcohol units a week less. They also went binge drinking less often compared with the people who did not use them.

The websites and programs studied included information on health risks of too much alcohol, how to calculate units of alcohol, and support resources. Some included interactive games and assignments or tips on how to build skills in motivation, refusing alcohol, and changing risk-taking behavior.

An example of this kind of website is [Rethinking Drinking](http://rethinkingdrinking.niaaa.nih.gov/)[](#_ftn1) from the National Institutes of Health (NIH).

The systematic review, along with a critical summary including more details from the Database of Reviews of Effects, is [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0031589/).[](#_ftn2)

Citation: Khadjesari Z, Murray E, Hewitt C, Hartley S, Godfrey C.  Can stand-alone computer-based interventions reduce alcohol consumption? A systematic review. Addiction 2011; 106(2): 267-282. Cited in: PubMed. [PMID 21083832](http://www.ncbi.nlm.nih.gov/pubmed/21083832)


