Featured review
---------------

![Image of group eating outside.](/core/assets/pmh/images/PMH_mediterranean.png)
Being overweight increases a person’s risk of heart disease and stroke (cardiovascular disease). Factors like high blood pressure and cholesterol levels are signs of increased risk for cardiovascular disease.

Losing weight is one of the main strategies to reduce these risks—but there are many, often contradictory, claims about what will work.

Two of the main types of diets that have been recommended to help overweight people reduce their risks are a low-fat/high-carbohydrate diet and the Mediterranean diet.

The Mediterranean diet is a moderate-fat diet, with:

-   a high intake of monounsaturated fat (particularly from olive oil),
-   high intake of plant proteins (like legumes),
-   lots of whole grains but low intake of refined grains and sweets, and
-   high intake of fish but low consumption of red meat.

The trials comparing a Mediterranean diet with a low-fat diet in overweight people were analyzed by an international team of researchers. The team was led by researchers from the Basel University Hospital in Switzerland.

After two years, people in the Mediterranean diet group lost an average of around 4.8 pounds (2.2kg). The people in the low-fat diet group lost less weight: an average of 1.2 pounds, or just over half a kilo.

People in the Mediterranean diet group also lowered cardiovascular risk factors like cholesterol levels and blood pressure more than people on low-fat diets managed to do.

The researchers’ summary of their review is [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0034482/).

Nordmann AJ, Suter-Zimmermann K, Bucher HC, Shai I, Tuttle KR, Estruch R, Briel M. Meta-analysis comparing mediterranean to low-fat diets for modification of cardiovascular risk factors. American Journal of Medicine 2011; 124(9): 841-851. [PMID 21854893](http://www.ncbi.nlm.nih.gov/pubmed/21854893)


