Featured review
---------------

![Image of man](/core/assets/pmh/images/PMH_manlaptop.jpg)

Website therapy for depression
==============================

Cognitive behavior therapy (CBT) is an umbrella term for particular types of psychological treatment. [This kind of therapy](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016283/) aims to help people cope with, and solve, their problems.

The goal is to learn to identify thoughts and behaviors that are having a negative effect on wellbeing—as well as techniques to change them.

Websites have been developed that help people work through the CBT process themselves, either alone or with some therapist support. There are also CBT books.

A team of researchers from around the world gathered as much of the original data as they could from trials of more or less "do it yourself" CBT to see whether people who are more depressed benefit. This type of systematic review is called an individual patient data meta-analysis.

The team was able to get more than half the trial data: a total of 2,470 people with mild to moderate depression, mostly in trials of CBT websites.

Their analysis suggested that using this kind of "low intensity" CBT could help reduce the severity of depression from moderate to mild.

To find out more, read:

-   The DARE\* analysis and summary of this [systematic review on depression](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0053166/%20);
-   The [full report of the review](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3582703/%20)

To see one of the websites included in this study, you can visit:

-   The [MoodGym](https://moodgym.anu.edu.au/welcome%20) and
-   See [the trial](http://www.ncbi.nlm.nih.gov/pubmed/20528705%20) of that website

You can find out more about CBT in this [fact sheet on cognitive behavior therapy](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016271/%20).

<span>*Citation:* Bower P, Kontopantelis E, Sutton A, Kendrick T, Richards DA, Gilbody S, Knowles S, Cuijpers P, Andersson G, Christensen H, Meyer B, Huibers M, Smit F, van Straten A, Warmerdam L, Barkham M, Bilich L, Lovell K, Liu ET. Influence of initial severity of depression on effectiveness of low intensity interventions: meta-analysis of individual patient data. BMJ 2013; 346: f540. [PMID 23444423](http://www.ncbi.nlm.nih.gov/pubmed/23444423)</span>

<span>\* *DARE is the Database of Reviews of Effects, produced by one of PubMed Health’s information partners, the [Centre for Reviews and Dissemination](http://www.york.ac.uk/inst/crd/%20). DARE identifies and analyzes the quality of systematic reviews of health care.*</span>

*By PubMed Health, 12 March 2013*

[More featured reviews](http://www.ncbi.nlm.nih.gov/feed/rss.cgi?ChanKey=pmhfeaturedreviews)

[How do we know what works?](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)


