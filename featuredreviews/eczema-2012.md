Featured review
---------------

![Image of pregnant woman.](/core/assets/pmh/images/PMH_pregnant.png)
Atopic dermatitis (eczema) is the most common allergy in babies and small children. If allergies run in a family, it could be worthwhile to try to prevent dermatitis when the risk is higher.

Reviewers from research institutes in Milan and Paris looked for trials testing whether or not probiotic supplements can prevent this type of dermatitis. Probiotics are "friendly bacteria" naturally found in yogurt. Probiotic tablets or liquid supplements can be used by pregnant women or by babies.

The reviewers found 14 trials, mostly done between 2007 and 2011. They found a roughly 20% reduction in the rate of atopic dermatitis (from around 34% in the children in these trials to 26%).

You can read a summary of their results [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0046855/%20).

You can read more about preventing allergies in babies and children [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004990/%20).

*Citation:* Pelucchi C, Chatenoud L, Turati F, Galeone C, Moja L, Bach JF, La Vecchia C. Probiotics supplementation during pregnancy or infancy for the prevention of atopic dermatitis: a meta-analysis. *Epidemiology* 2012; 23(3): 402-414. [PMID 22441545](http://www.ncbi.nlm.nih.gov/pubmed/22441545)


