Featured review
---------------

![Image for featured review.](/core/assets/pmh/images/PMH_sleeping.jpg)
Obstructive sleep apnea (OSA) can reduce sleep quality – it can bring with it not just daytime sleepiness, but a higher risk of accidents and some illnesses.

The US Agency for Healthcare Research and Quality (AHRQ) comprehensively reviewed the research on diagnosis and treatment options for OSA.

See AHRQ’s guide for consumers [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016158/%20). 

Or read their guide for clinicians [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016159/).

The full text of their research review is [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016344/).

Citation: Balk EM, Moorthy D, Obadan NO, Patel K, Ip S, Chung M, Bannuru RR, Kitsios GD, Sen S, Iovin RC, Gaylor JM, D’Ambrosio C, Lau J. Diagnosis and Treatment of Obstructive Sleep Apnea in Adults [Internet]. Rockville (MD): Agency for Healthcare and Research Quality (US); 2011 Jul [cited 2011 Nov 3]. Available from: <http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016344/>

Interesting in reading more about OSA? There is also an article [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016269/) from the Institute for Quality and Efficiency in Health Care (IQWiG).


