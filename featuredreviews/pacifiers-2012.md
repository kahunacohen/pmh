Featured review
---------------

![Image of mother and baby.](/core/assets/pmh/images/PMH_mom_baby.png)
Pacifier, dummy, binky: many people feel very strongly about them—especially the potential effect on breastfeeding. But what’s the evidence about whether or not they have an impact?

Reviewers from the Cochrane Collaboration in Canada, India, and Malaysia looked for trials to answer this question. They found trials in just over 1,300 babies showing that using a pacifier did not reduce exclusive breastfeeding after a few months.

You can read a summary of their results [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0014360/%20%20%20).

This review looks at the specific question of pacifiers and breastfeeding. It wasn’t looking at questions like effects on development of teeth or risk of spreading infections to the ear. And the babies were full term.

You can read about the importance of pacifiers for tube-fed preterm babies [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0010837/).

*Citation:* Jaafar SH, Jahanfar S, Angolkar M, Ho JJ. Effect of restricted pacifier use in breastfeeding term infants for increasing duration of breastfeeding. *Cochrane Database of Systematic Reviews* 2012, Issue 7. Art. No.: CD007202. DOI: 10.1002/14651858.CD007202.pub3. [Link to Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD007202.pub3/full)


