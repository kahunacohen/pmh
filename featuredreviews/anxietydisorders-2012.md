Featured review
---------------

![Image of female alone in woods.](/core/assets/pmh/images/PMH_woods.png)
Anxiety disorders are very common and difficult to deal with. The term “anxiety disorders” covers many conditions, including panic and phobias.

One option for treating anxiety disorders is self-help based on therapy principles. Using a book or a website to help, a person tackles the kinds of exercises that would be used in therapy, but they do it on their own, perhaps with some email or phone support from a therapist.

Researchers from Cardiff University in Wales were able to find and analyze 31 trials testing the effects of this kind of self-help. The best-studied conditions were panic disorder and social phobia. People with social phobia often experience extreme anxiety when they encounter or face other people.

The researchers found that websites, books, and other materials based on cognitive-behavioral therapy (CBT) could help some people. You can read a quick overview of CBT [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016283/) or in more depth [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016271/).

The Cardiff researchers’ summary of their review is [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0034334/).

Citation: Lewis C, Pearce J, Bisson JI. Efficacy, cost-effectiveness and acceptability of self-help interventions for anxiety disorders: systematic review. British Journal of Psychiatry 2012; 200(1): 15-21. [PMID 22215865](http://www.ncbi.nlm.nih.gov/pubmed/22215865)

If you would like to read more background information about issues related to computerized CBT for anxiety, a review was published by England’s National Institute for Health Research (NIHR) in 2006. It is summarized in detail [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0015140/), with full text accessible [here](http://www.hta.ac.uk/fullmono/mon1033.pdf%20).


