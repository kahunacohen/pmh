Featured review
---------------

![Image of family.](/core/assets/pmh/images/PMH_beach.png)
Children’s conduct problems can be extremely challenging for parents. These disorders do not include attention deficit or attention deficit hyperactive disorder (ADHD), although sometimes children have both.

Parenting programs teach behavior principles and skills for parenting children with conduct problems—like anger management and positive reinforcement of good behavior—plus provide opportunities for rehearsing them.

These group programs also aim to break thinking patterns in parents that make problems worse—and life unhappier for the parents, too. An example of this kind of cognitive approach is stopping “all or nothing” thinking that leads to stress and inability to cope (like thinking “I am a bad parent”).

A Cochrane review found 13 trials of this kind of group program. The authors concluded there can be benefits for parents and children from participating. Read their summary [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0033931/).

Citation: Furlong M, McGilloway S, Bywater T, Hutchings J, Smith SM, Donnelly M. Behavioural and cognitive‐behavioural group‐based parenting programmes for early‐onset conduct problems in children aged 3 to 12 years. *Cochrane Database of Systematic Reviews* 2012, Issue 2. Art. No.: CD008225. DOI: 10.1002/14651858.CD008225.pub2. [Link to Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD008225.pub2/full).


