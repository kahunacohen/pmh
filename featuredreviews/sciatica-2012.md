Featured review
---------------

![Image of man fishing with son for featured review.](/core/assets/pmh/images/PMH_fishing.png)
Sharp, shooting or burning pain down a leg—sciatica might affect about 3% of men and more than 1% of women.

England’s National Institute for Health Research (NIHR) commissioned a review of the evidence on the various treatment options for sciatica, including medications and surgery.

Read the summary [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0033970/) at PubMed Health.

Citation: Lewis R, Wiliams N, Matar HE, Din N, Fitzsimmons D, Philips C, et al. The clinical effectiveness and cost-effectiveness of management strategies for sciatica: systematic review and economic model. Health Technol Assess 2011;15(39). Full text available from <http://www.hta.ac.uk/fullmono/mon1539.pdf>


