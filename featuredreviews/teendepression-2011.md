Featured review
---------------

![Image of teenager for featured review.](/core/assets/pmh/images/89692194_sm.jpg)
It’s starting to look encouraging, according to Cochrane reviewers: some depression prevention programs for young people could be working. Read the latest results [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012012/).

Other relevant reading at PubMed Health includes –

[Fact sheet on preventing depression in children and young people](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005126/%20)

[Depression: Strategies for families and friends](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005028/)

[Cognitive behavioral therapy at a glance](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016283/)

Citation: Merry SN, Hetrick SE, Cox GR, Brudevold-Iversen T, Bir JJ, McDowell H. Psychological and educational interventions for preventing depression in children and adolescents. Cochrane Database of Systematic Reviews: Plain Language Summaries [Internet]. 2011 Dec 7 [cited 2011 Dec 8]; (12): CD003380. Available from [http://](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012012/)[www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012012/](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012012/)


