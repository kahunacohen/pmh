Featured review
---------------

![Image of teens for featured review.](/core/assets/pmh/images/PMH_5teens.png)
It’s a very common procedure for young people. Wisdom teeth are surgically removed when they are causing problems like pain and swelling.

But very often when wisdom teeth are removed, they are not causing any symptoms (asymptomatic). Impacted means they have not broken through.

Researchers from the Radboud University of Nijmegen Medical Center in the Netherlands have updated their Cochrane review on surgery for asymptomatic impacted wisdom teeth.

You can read about their research results [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012379/).

Find out more about wisdom teeth removal in this [fact sheet](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004981/%20).

*Citation:* Mettes TDIRKG, Ghaeminia H, Nienhuijs MEL, Perry J, van der Sanden WJM, Plasschaert A. Surgical removal versus retention for the management of asymptomatic impacted wisdom teeth. Cochrane Database of Systematic Reviews 2012, Issue 6. Art. No.: CD003879. DOI: 10.1002/14651858.CD003879.pub3. [Link to Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD003879.pub3/full)


