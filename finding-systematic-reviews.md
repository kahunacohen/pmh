Finding systematic reviews at PubMed Health
===========================================

PubMed Health specializes in [systematic reviews of clinical effectiveness research](/pubmedhealth/aboutcer/%20). We include:

-   <span>Plain language summaries and abstracts of </span>[Cochrane reviews](http://www.cochrane.org/)<span>: </span>[Example](/pubmedhealth/PMH0054481/)
-   Abstracts (short technical summaries) of systematic reviews in [DARE](http://www.ncbi.nlm.nih.gov/pubmedhealth/about/DARE/), the Database of Abstracts of Reviews of Effects—many of them with a critical summary of the review: [Example](/pubmedhealth/PMH0054761/)
-   <span>Full texts of reviews from a growing group of </span>[public agencies](/pubmedhealth/about/)<span>: </span>[Example](/pubmedhealth/PMH0086840/)
-   Information developed by [public agencies](/pubmedhealth/about/) for consumers and clinicians that is based on systematic reviews: [Example](/pubmedhealth/PMH0032825/)
-   Methods resources about the best research and statistical techniques for systematic reviews and clinical effectiveness research: [Example](/pubmedhealth/PMH0077740/)

We include systematic reviews on the effects of healthcare or clinical effectiveness research methods published from 2003 onwards. Systematic reviews were no longer added to DARE after March 2015.

In May 2016, there were over 40,000 systematic reviews at PubMed Health. Cochrane reviews are added when they are published, but reviews by public health agencies usually have a processing timelag.

You can keep up with new and updated information at PubMed Health on our [What’s New page](/pubmedhealth/new/), through [our blog](/pubmedhealth/blog/) or on [Twitter](https://twitter.com/PubMedHealth), [Facebook](https://www.facebook.com/PubMedHealth) and [Google+](https://plus.google.com/107599362190099097644/posts).

You can use My NCBI to get email alerts when a Cochrane review or National Cancer Institute evidence-based information is updated. Find out about this and other alerts for PubMed Health [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/05/update-alerts-Cochrane-NCI). 

Systematic reviews at PubMed and PubMed Health
----------------------------------------------

[PubMed](http://www.ncbi.nlm.nih.gov/pubmed) is the database of citations of biomedical literature from the National Library of Medicine ([NLM](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutnlm/)). Not all the reviews included in PubMed Health are in PubMed. This is because DARE did not restrict itself to journals included in PubMed.

However, PubMed includes systematic reviews from earlier than 2003, as well as more recent reviews (with the exception of Cochrane reviews). There are also systematic reviews on topics other than the effects of healthcare—reviews of preclinical studies, prognosis, or genetic associations, for example.

When you search in PubMed Health, there is a box on the right showing "**Systematic reviews in PubMed**." This shows the results of a simultaneous search of the  PubMed using the ["Clinical Queries" filter](http://www.nlm.nih.gov/bsd/pubmed_subsets/sysreviews_strategy.html%20). This is a filter looking for studies that might be a systematic review or a guideline: many if not most results of this search will not be a systematic review. One of PubMed Health's partners, the Centre for Reviews and Dissemination (CRD), maintains a list of other filters for systematic reviews that you can use in PubMed [here](https://sites.google.com/a/york.ac.uk/issg-search-filters-resource/filters-to-identify-systematic-reviews).

Narrowing down your searches in PubMed Health
---------------------------------------------

There are several quick searches for new and updated information at our [What’s New page](/pubmedhealth/new/).

You can narrow down your search in PubMed Health yourself using one of these filters:

-   Systematic reviews only: pmh\_sr[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_sr%5Bsb%5D)
-   Systematic reviews in the last week: pmh\_sr\_week[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_sr_week%5Bsb%5D)
-   Systematic reviews in the last month: pmh\_sr\_month[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_sr_month%5Bsb%5D)
-   DARE reviews only: pmh\_dare[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_dare%5Bsb%5D)
-   DARE reviews in the last week: pmh\_dare\_week[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_dare_week%5Bsb%5D)
-   DARE reviews in the last month: pmh\_dare\_month[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_dare_month%5Bsb%5D)
-   Cochrane reviews only: pmh\_cc[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_cc%5Bsb%5D)
-   Cochrane reviews in the last week: pmh\_cc\_week[sb] [Example ](/pubmedhealth/?term=cancer+AND+pmh_cc_week%5Bsb%5D)
-   Cochrane reviews in the last month: pmh\_cc\_month[sb] [Example ](/pubmedhealth/?term=cancer+AND+pmh_cc_month%5Bsb%5D)
-   Reviews other than DARE and Cochrane: pmh\_sr\_other[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_sr_other%5Bsb%5D)
-   Other reviews in the last week: pmh\_sr\_other\_week[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_sr_other_week%5Bsb%5D)
-   Other reviews in the last month: pmh\_sr\_other\_month[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_sr_other_month%5Bsb%5D)
-   Information for consumers only (includes Cochrane): pmh\_cons[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_cons%5Bsb%5D)  
-   Information for consumers in the last week: pmh\_cons\_week[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_cons_week%5Bsb%5D)
-   Information for consumers in the last month: pmh\_cons\_month[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_cons_month%5Bsb%5D)
-   Information for clinicians only: pmh\_clin[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_clin%5Bsb%5D)
-   Information for clinicians in the last week: pmh\_clin\_week[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_clin_week%5Bsb%5D)
-   Information for clinicians in the last month: pmh\_clin\_month[sb] [Example](/pubmedhealth/?term=cancer+AND+pmh_clin_month%5Bsb%5D)

Note that these filters only work in PubMed Health, not PubMed.

Full texts of systematic reviews
--------------------------------

We include many full texts within PubMed Health. Many additional full texts are available at another NLM database called [PMC](http://www.ncbi.nlm.nih.gov/pmc/). Articles at journal websites are sometimes free. 

<span>PubMed Health links directly out to the journal website, as well as to PMC if the review is included in free full text there: </span>[Example](/pubmedhealth/PMH0072109/)

*PubMed Health*
 *10 May 2016*


