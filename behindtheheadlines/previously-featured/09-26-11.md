### Featured UK headline analysis

#### [Pollution 'linked to heart attack risk'](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2011-09-21-pollution-linked-to-heart-attack-risk-/)

21 September 2011

![Image for UK NHS headline](/projects/pubmedhealth/bth/img/PMH_exhaust.png)
"<span class="Apple-style-span" style="font-size: 13px; ">Traffic fumes can trigger heart attacks, say researchers,” *The Guardian* reported today. It said that “breathing in large amounts of traffic fumes can trigger a heart attack up to six hours after exposure”. </span><span class="Apple-style-span" style="font-size: 13px; ">This large study investigated the relationship between the risk of having a heart attack and exposure to different traffic pollutants. Researchers analysed nearly 80,000 heart attacks and the persons’ exposure to air pollution leading up to the attack.</span>

### In the US

#### [Car pollution fast-tracks heart attacks, study says](http://www.cbsnews.com/8301-504763_162-20109629-10391704.html)

CBS News

What triggers a heart attack? Studies have linked unhealthy eating, a sedentary lifestyle, and smoking to heart attack risk.

### Related Info

#### [Is it a heart attack?](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005033/)

PubMed Health

Information to help you recognize the signs.


