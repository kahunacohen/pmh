### Featured UK headline analysis

#### [Can exercise stop mental decline?](/~/behindtheheadlines/news/2012-04-24-can-exercise-stop-mental-decline/)

24 April 2012

![Image for UK NHS headline](/core/assets/pmh/images/PMH_woman_weights.jpg)Exercise can dramatically alter the rate of mental decline in elderly people with early signs of dementia, the Daily Express reported today. The news reports are based on a small study that compared how different types of exercise may affect the mental ability of elderly women with “probable” mild cognitive impairment (MCI). MCI is a risk factor for developing dementia, although it does not always lead to the condition. During a six-month trial, elderly women were asked to regularly perform either aerobic exercise, muscle-strengthening “resistance training” such as weights

### In the US

#### [Pumping Iron to Prevent Dementia?](http://abcnews.go.com/blogs/health/2012/04/23/pumping-iron-to-prevent-dementia/)

ABC News

Resistance training could be an important part of reversing memory decline in elderly women with mild memory problems

### Related Info

#### [What do trials of exercise conclude?](/~/PMH0041382/)

PubMed Health

Trials on increasing fitness in older people and cognitive decline are summed up here


