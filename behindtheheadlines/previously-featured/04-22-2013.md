### Featured UK headline analysis

#### [No proof found that depression is 'contagious'](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2013-04-22-no-proof-found-that-depression-is-contagious/)

22 April 2013

![Image of women](/core/assets/pmh/images/PMH_roommates.jpg)'Can you catch depression?', the Mail Online website asks on the strength of new US research into the concept of 'cognitive vulnerability'. Cognitive vulnerability is where unhelpful patterns of thinking can increase the risk of a person developing conditions such as depression. The researchers in this study were interested in the idea that cognitive vulnerability may be 'contagious'. The study followed roughly 100 pairs of roommates at a US university for the first six months of their freshman (first) year. They wanted to see if one student's cognitive vulnerability might influence the cognitive vulnerability of their new roommate.

### In the US

#### [Depression Risk Factor, Cognitive Vulnerability, Could Be Contagious, Study Suggests](http://www.huffingtonpost.com/2013/04/18/depression-contagious-cognitive-vulnerability_n_3111940.html)

The Huffington Post

A known risk factor for depression could be "contagious" under the right circumstances, a small new study suggests.

### Related Info

#### [What can you do when a friend is depressed?](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005028/)

PubMed Health

Learn how to tell and what you can do to help


