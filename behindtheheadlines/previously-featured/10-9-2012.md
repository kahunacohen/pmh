### Featured UK headline analysis

#### [Can tomatoes prevent a stroke?](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2012-10-09-can-tomatoes-prevent-a-stroke/)

9 October 2012

![Image of tomatoes. ](/core/assets/pmh/images/PMH_tomatoes.jpg)"Tomatoes are 'stroke preventers'," BBC News has claimed. The news is based on a study looking at the levels of various chemicals called carotenoids in men’s blood and their long-term risk of stroke.Carotenoids are naturally occurring chemicals which give fruit and vegetables their colour. They can act as antioxidants. Antioxidants are believed to help protect against cell damage from molecules known as "free radicals" and "singlet molecular oxygen". Antioxidants are thought to work by reacting with an unstable molecule and bringing it under control. Some have suggested that antioxidants may have a protective effect against stroke

### In the US

#### [Tomato compound may cut men's stroke risk](http://www.foxnews.com/health/2012/10/09/tomato-compound-may-cut-men-stroke-risk/)

FOX News

Eating tomatoes and other foods rich in the antioxidant lycopene may reduce men's risk of stroke, new research suggests.

### Related Info

#### [Lycopenes, cholesterol and blood pressure](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0031659/)

PubMed Health

Effect on stroke isn't known, but find out what the effect of dietary lycopenes is on some known risk factors.


