### Featured UK headline analysis

#### [Paralysed rats taught to walk again](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2012-06-01-paralysed-rats-taught-to-walk-again/)

1 June 2012

![Image of scientists.](/core/assets/pmh/images/PMH_chemist_2.png)An experimental rehabilitation method has enabled paralysed rats to walk again, scientists have today revealed. The remarkable feat has featured heavily in today’s news, which has emphasised both the leap forward it represents and the fact that it is still far too early to consider it as a treatment for humans. During the study, rats had two partial cuts made in their spinal cords. These cut all direct signals for controlling their hind legs, but left gaps where nerves might potentially form new connections. Researchers then gave the rats a course of drugs injected into the spine, electrical nerve stimulation and physical training

### In the US

#### [Injured rats walk, climb thanks to spinal nerve stimulation](http://www.cbsnews.com/8301-501367_162-57444783/experiment-lets-spine-injured-rats-walk-climb/)

CBS News

Many scientists are working on treatments to help people with spinal cord injuries walk. Now there's a striking new demonstration

### Related Info

#### [Drugs and spinal cord injury](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0010819/)

PubMed Health

A systematic review shows benefit from a steroid at the time of the injury


