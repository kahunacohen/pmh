### Featured UK headline analysis

#### [Heart attack symptoms vary by gender](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2012-02-22-heart-attack-symptoms-vary-by-gender/)

22 February 2012

![Image for UK NHS headline](/core/assets/pmh/images/PMH_pensive_woman.png)“Heart symptoms differ in women,” BBC News has today reported. The broadcaster says that women having a heart attack are less likely to experience chest pain, compared with men. A heart attack can cause a range of different symptoms, from crushing chest pains to tingling in the limbs, and feelings of breathlessness or nausea. Given the variations in possible symptoms, US researchers set out to examine just how common chest pain and discomfort is for each gender, and whether it indicated an increased risk that a patient might die. To do so, they examined records on more than 1 million US men and women who suffered from a heart attack

### In the US

#### [Younger women with heart attacks face higher death risk](http://www.boston.com/2012/02/21/heartattack/zRfDRKGU9FwCC7tQ0FgGEN/story.html)

The Boston Globe

While younger women rarely have heart attacks, those who do face a greater risk of dying while in the hospital compared with men the same age

### Related Info

#### [The signs of a heart attack](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005033/)

PubMed Health

How to know if it's a heart attack and what to do


