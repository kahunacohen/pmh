### Featured UK headline analysis

#### ['Morning sickness' linked to healthier babies](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2014-07-30-morning-sickness-linked-to-healthier-babies/)

30 July 2014

![Photo of woman drinking tea at computer](/core/assets/pmh/images/morning-sickness-photo-for-BtH-2014-08-04.jpg)“Morning sickness isn't all bad news: Women battling the condition may have 'healthier, more intelligent babies’,” the Mail Online reports. The news is based on the results of a systematic review that looked at the effects of “morning sickness”. Health professionals prefer the term “nausea and vomiting in pregnancy (NVP)” because, as many pregnant women can attest, symptoms can occur at any time. The researchers were interested in whether NVP was associated with better pregnancy outcomes. The researchers identified 10 observational studies. All 10 reported a protective effect of NPV, such as a reduced risk of miscarriage, birth defects and premature birth. There was also evidence of an association with

### In the US

#### [The Upside to Morning Sickness](http://online.wsj.com/articles/the-upside-to-morning-sickness-1406563253)

The Wall Street Journal

Could morning sickness have an upside for pregnant women and their babies? A new study found that women with symptoms of nausea and vomiting during pregnancy

### Related Info

#### [Interventions for nausea and vomiting in early pregnancy](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0014524/)

PubMed Health

Find out what trials have been done to see what might work


