### Featured UK headline analysis

#### [Smokers recall visual health warnings better](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2012-06-16-smokers-recall-visual-health-warnings-better/)

16 June 2012

![Image of Stop Smoking post-it.](/core/assets/pmh/images/PMH_corkboard.png)“Graphic warning labels on cigarette packs work better” than written warnings, the BBC reported. The broadcaster said researchers have found that graphic images of health problems are more effective than written warnings when placed on cigarette packets. This US trial enrolled 200 current smokers and randomly showed them a static cigarette advertisement that had either a standard text warning or a photographic warning label. The study used eye-tracking technology to monitor how the participants viewed the images, and then later assessed their recall of the health warning.

### In the US

#### [Graphic Cigarette Warning Labels Stick Better in Your Memory](http://healthland.time.com/2012/06/15/graphic-cigarette-warning-labels-stick-better-in-your-memory/)

TIME

If the U.S. Food and Drug Administration (FDA) has its way, warning labels on cigarette packs and advertisements are going to get a lot more grisly this fall.

### Related Info

#### [Health risks & how to quit](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0032815/)

PubMed Health

Up-to-date information from the National Cancer Institute


