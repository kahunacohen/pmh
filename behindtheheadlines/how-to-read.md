How to read health news
=======================

By Dr Alicia White

*From "Behind the Headlines", provided by NHS Choices (from England's National Health Service).*

If you’ve just read a health-related headline that has caused you to spit out your morning coffee (“Coffee causes cancer” usually does the trick), it’s always best to follow the Blitz slogan: “Keep Calm and Carry On”. On reading further, you’ll often find the headline has left out something important, such as, “Injecting five rats with really highly concentrated coffee solution caused some changes in cells that might lead to tumours eventually. (Study funded by The Association of Tea Marketing)”.

The most important rule to remember is: don’t automatically believe the headline. It is there to draw you into buying the paper and reading the story. Would you read an article called, “Coffee pretty unlikely to cause cancer, but you never know”? Probably not.

To avoid spraying your newspaper with coffee in the future, you need to analyse the article to see what it says about the research it is reporting on. Bazian (the company I work for) has appraised hundreds of articles for Behind The Headlines on NHS Choices, and we’ve developed the following questions to help you figure out which articles you’re going to believe and which you’re not.

### Does the article support its claims with scientific research?

Your first concern should be the research behind the news article. If an article touts a treatment or some aspect of your lifestyle that is supposed to prevent or cause a disease, but doesn’t give any information about the scientific research behind it, then treat it with a lot of caution. The same applies to research that has yet to be published.

### Is the article based on a conference abstract?

Another area for caution is if the news article is based on a conference abstract. Research presented at conferences is often at a preliminary stage and usually hasn’t been scrutinised by experts in the field. Also, conference abstracts rarely provide full details about methods, making it difficult to judge how well the research was conducted. For these reasons, articles based on conference abstracts should be no cause for alarm. Don’t panic or rush off to your doctor.

### Was the research in humans?

Quite often, the “miracle cure” in the headline turns out to have only been tested on cells in the laboratory or on animals. These stories are regularly accompanied by pictures of humans, which creates the illusion that the miracle cure came from human studies. Studies in cells and animals are crucial first steps and should not be undervalued. However, many drugs that show promising results in cells in laboratories don’t work in animals, and many drugs that show promising results in animals don’t work in humans. If you read a headline about a drug or food “curing” rats, there is a chance it might cure humans in the future, but unfortunately a larger chance that it won’t. So there is no need to start eating large amounts of the “wonder food” featured in the article.

### How many people did the research study include?

In general, the larger a study the more you can trust its results. Small studies may miss important differences because they lack statistical “power”, and are also more susceptible to finding things (including things that are wrong) purely by chance.

You can visualise this by thinking about tossing a coin. We know that if we toss a coin the chance of getting a head is the same as that of getting a tail – 50/50. However, if we didn’t know this and we tossed a coin four times and got three heads and one tail, we might conclude that getting heads was more likely than tails. But this chance finding would be wrong. If we tossed the coin 500 times - i.e. gave the experiment more "power" - we'd be more likely to get a heads/tails ratio close to 50/50, giving us a better idea of the true odds. When it comes to sample sizes, bigger is usually better. So when you see a study conducted in a handful of people, treat it with caution.

### Did the study have a control group?

There are many different types of studies appropriate for answering different types of questions. If the question being asked is about whether a treatment or exposure has an effect or not, then the study needs to have a control group. A control group allows the researchers to compare what happens to people who have the treatment/exposure with what happens to people who don’t. If the study doesn’t have a control group, then it’s difficult to attribute results to the treatment or exposure with any level of certainty.

Also, it’s important that the control group is as similar to the treated/exposed group as possible. The best way to achieve this is to randomly assign some people to be in the treated/ exposed group and some people to be in the control group. This is what happens in a randomised controlled trial (RCT) and is why RCTs are considered the “gold standard” for testing the effects of treatments and exposures. So when reading about a drug, food or treatment that is supposed to have an effect, you want to look for evidence of a control group, and ideally, evidence that the study was an RCT. Without either, retain some healthy scepticism.

### Did the study actually assess what’s in the headline?

This one is a bit tricky to explain without going into a lot of detail about things called proxy outcomes. Instead, bear in mind this key point: the research needs to have examined what is being talked about in the headline and article. (Somewhat alarmingly, this isn’t always the case.)

For example, you might read a headline that claims, “Tomatoes reduce the risk of heart attacks”. What you need to look for is evidence that the study actually looked at heart attacks. You might instead see that the study found that tomatoes reduce blood pressure. This means that someone has extrapolated that tomatoes must also have some impact on heart attacks, as high blood pressure is a risk factor for heart attacks. Sometimes these extrapolations will prove to be true, but other times they won’t. Therefore if a news story is focusing on a health outcome that was not examined by the research, treat it with a pinch of salt.

### Who paid for and conducted the study?

This is a somewhat cynical point, but one that’s worth making. The majority of trials today are funded by manufacturers of the product being tested – be it a drug, vitamin cream or foodstuff. This means they have a vested interest in the results of the trial, which can potentially affect what the researchers find and report in all sorts of conscious and unconscious ways. This is not to say that all manufacturer-sponsored trials are unreliable. Many are very good. However, it’s worth seeing who funded the study to sniff out a potential conflict of interest.

### Should you “shoot the messenger”?

Overblown claims might not necessarily be down to the news reporting itself. Although journalists can sometimes misinterpret a piece of research, at other times the researchers (or other interested parties) over-extrapolate, making claims their research doesn’t support. These claims are then repeated by the journalists.

Given that erroneous claims can come from a variety of places, don’t automatically assume they come from the journalist. Instead, use the questions above to figure out for yourself what you’re going to believe and what you’re not.

[Latest Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/)


