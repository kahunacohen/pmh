About drug names and drug classes
=================================

Drug names
----------

![Photo of man examining medications](/core/assets/pmh/images/about-drugs.png)

A drug can have two names: a **substance name** and a **brand name**.

A **substance name** is a chemical. It is also called the active ingredient. If there is more than one substance in a drug, it is called a combination drug.

Drug manufacturers choose the **brand names** of their products. There can be many brands of a particular drug.

The brand names may be better known and are sometimes easier to remember. For example, "aspirin" is a registered brand name in some countries. The substance name for aspirin is "acetylsalicylic acid" or ASA.

You can search for information about drugs in PubMed health using either substance names or brand names.

Drug classes
------------

A prescribed drug also belongs to one or more drug classes. A drug class is a group of drugs that have something in common. They are similar in some way, but they are not identical.

Drugs can be in a class with other drugs because:

-   The drugs are related by their chemical structure.
     *Example:* Aspirin is a salicylate. That is a chemical found in plants, for example, in willow tree bark.
-   The drugs work in the same way.
     *Example:* Aspirin reduces swelling (inflammation) by interfering with a particular enzyme. So it belongs to a drug class called nonsteroidal anti-inflammatory drugs (NSAIDs).
-   The drugs are used for the same purpose.|*Example:* Aspirin is used to reduce fever. Drugs that treat fever are called anti-pyretic drugs or anti-pyretics.

*You can find information by searching PubMed Health for drug substance names, brand names or drug classes.*


