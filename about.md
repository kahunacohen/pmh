About PubMed Health
-------------------

PubMed Health provides information for consumers and clinicians on prevention and treatment of diseases and conditions.

[PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/) specializes in reviews of clinical effectiveness research, with easy-to-read summaries for consumers as well as full technical reports. [Clinical effectiveness research](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/) finds answers to the question “What works?” in medical and health care.

PubMed Health is a service provided by the National Center for Biotechnology Information (NCBI) at the U.S. National Library of Medicine (NLM).

Follow us on Twitter at [@PubMedHealth](http://twitter.com/PubMedHealth).

You can +1, share, and follow us on [Google+](https://plus.google.com/107599362190099097644).

You can also subscribe to our RSS feeds: [Featured Reviews](http://www.ncbi.nlm.nih.gov/feed/rss.cgi?ChanKey=pmhfeaturedreviews) and [Behind the Headlines](http://www.ncbi.nlm.nih.gov/feed/rss.cgi?ChanKey=BehindTheHeadlines).

[Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/), an evidence-based analysis of health stories in the news, is from England’s National Health Service [(NHS Choices)](http://www.nhs.uk/)<span style="line-height: 20.4px;">. (More about [Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/about/).)</span>

### **Who we are**

The U.S. [National Library of Medicine](http://www.nlm.nih.gov/about/index.html) (NLM) is the world's largest medical library. It has millions of books and journals about all aspects of medicine and health care on its shelves. Its electronic services deliver trillions of bytes of data to millions of users every day.

The NLM was founded in 1836 and is part of the National Institutes of Health in Bethesda, Maryland.

The [National Center for Biotechnology Information](http://www.ncbi.nlm.nih.gov/%20) (NCBI) is a division of the NLM. It creates resources for researchers, particularly large-scale research in human genetics.

The NCBI also provides public access to information through resources like [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/). PubMed includes abstracts—short technical summaries—of more than 26 million scientific articles in medicine and health.

### **Our information**

PubMed Health is based on systematic reviews of clinical trials. These clinical effectiveness reviews can show what treatments and prevention methods have been proven to work—and what remains unknown.

PubMed Health provides summaries and full texts of selected systematic reviews in one place. The reviews were generally published or updated from 2003. There is also information for consumers and clinicians based on those reviews.

Our Methods Resources collection includes documents about the best research and statistical techniques for systematic reviews and effectiveness research. Learn more about this and other resources for researchers [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/researchers/).

A search on PubMed Health runs simultaneously in PubMed. A filter is used to identify all the indexed scientific articles at the NLM that might be systematic reviews. This search includes articles from before 2003.

Information partners selected by PubMed Health to contribute their clinical effectiveness information are:

-   Agency for Healthcare Research and Quality (US) [(AHRQ)](http://www.ahrq.gov/)
-   Canadian Agency for Drugs and Technologies in Health [(CADTH)](http://www.cadth.ca/)
-   Centre for Reviews and Dissemination [(CRD)](http://www.york.ac.uk/inst/crd/)
-   [Cochrane](http://www.cochrane.org/)
-   Department of Veterans Affairs' Evidence-based Synthesis Program from the Veterans Health Administration R&D [(VA ESP)](http://www.hsrd.research.va.gov/publications/esp/)
-   German Institute for Quality and Efficiency in Health Care ([IQWiG](https://www.iqwig.de/en/home.2724.html))
-   National Cancer Institute [(NCI)](http://www.cancer.gov/)
-   <span style="background-color: transparent;">National Institute for Health and Care Excellence guidelines program </span>[(NICE)](http://www.nice.org.uk/)
-   National Institute for Health Research (NIHR) Health Technology Assessment Programme ([NIHR HTA](http://www.nets.nihr.ac.uk/programmes/hta))
-   Oregon Health and Science University's Drug Effectiveness Review Project [(DERP)](http://www.ohsu.edu/xd/research/centers-institutes/evidence-based-policy-center/derp/)
-   Swedish Council on Health Technology Assessment [(SBU)](http://www.sbu.se/en/)

The methods resources included in PubMed Health are contributed by:

-   Agency for Healthcare Research and Quality (US) ([AHRQ](http://www.ahrq.gov))
-   [Cochrane](http://www.cochrane.org%20)
-   German Institute for Quality and Efficiency in Health Care ([IQWiG](https://www.iqwig.de/en/home.2724.html%20))
-   National Institute for Health and Care Excellence ([NICE](http://www.nice.org.uk%20))
-   National Institute for Health Research (NIHR) Health Technology Assessment Programme ([NIHR HTA](http://www.nets.nihr.ac.uk/programmes/hta%20))
-   Institute of Medicine ([IOM](http://iom.nationalacademies.org%20))

PubMed Health also includes information from the National Heart, Lung, and Blood Institute's (NHLBI) [Health Topics](http://www.nhlbi.nih.gov/health/health-topics/by-alpha/).

Definitions used in PubMed Health are sometimes shortened. They come from:

-   National Institutes of Health (NIH)
    -   [Computer Retrieval of Information on Scientific Projects](http://www.nlm.nih.gov/research/umls/sourcereleasedocs/current/CSP/)
    -   <span style="line-height: 20.4px; background-color: transparent;">[Stem Cell Glossary](http://stemcells.nih.gov/info/pages/glossary.aspx)</span>
-   NIH <span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Cancer Institute (NCI)</span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[NCI Dictionary of Cancer Terms](http://www.cancer.gov/publications/dictionaries/cancer-terms)</span></span>
    -   [NCI Dictionary of Genetics Terms](http://www.cancer.gov/publications/dictionaries/genetics-dictionary)
    -   [NCI Thesaurus](http://ncit.nci.nih.gov/)
    -   <span style="line-height: 20.4px; background-color: transparent;">[PDQ](http://www.cancer.gov/publications/pdq)</span>[® - NCI's Comprehensive Cancer Database](http://www.cancer.gov/publications/pdq)
    -   <span style="line-height: 20.4px; background-color: transparent;">NCI's Surveillance, Epidemiology and End Results (SEER) Program </span><span style="line-height: 20.4px; background-color: transparent;">– [SEER Training Web Site Glossary of Terms](http://training.seer.cancer.gov/glossary.html)</span>
-   NIH <span style="line-height: 20.4px; background-color: transparent;">– National Eye Institute (NEI) </span>
    -   [Information for Healthy Vision: Diabetic Eye Disease Glossary](https://nei.nih.gov/diabetes/content/english/glossary)
    -   <span style="line-height: 20.4px;">[Information for Healthy Vision: Glaucoma Glossary](https://nei.nih.gov/glaucoma/content/english/glossary)</span>
-   NIH <span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Heart, Lung, and Blood Institute (NHLBI) <span style="background-color: transparent;">–</span><span style="background-color: transparent;"> [Health Topics A-Z](http://www.nhlbi.nih.gov/health/health-topics/by-alpha/) </span></span>
-   <span style="background-color: transparent;">NIH </span><span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Human Genome Research Institute (NHGRI) </span>
    -   [Chromosome Abnormalities](https://www.genome.gov/11508982/chromosome-abnormalities-fact-sheet/)
    -   <span style="background-color: transparent;">[<span style="background-color: transparent;">Talking Glossary of Genetic Terms</span>](http://www.genome.gov/Glossary/)</span>
-   NIH <span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Institute of Allergy and Infectious Diseases (NIAID) <span style="background-color: transparent;">– [Diseases & Conditions](https://www.niaid.nih.gov/diseases-conditions/all)</span></span>
-   NIH <span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Institute of Arthritis and Musculoskeletal and Skin Diseases (NIAMS) <span style="background-color: transparent;">– [Glossaries appended to articles](http://www.niams.nih.gov/health_info/)</span></span>
-   NIH <span style="line-height: 20.3999996185303px;">– National Institute of Child Health and Human Development (NICHD) </span><span style="line-height: 20.3999996185303px;">– [A to Z Health & Human Development Topics](http://www.nichd.nih.gov/health/topics/Pages/index.aspx)</span>
-   NIH <span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK)</span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Anatomy A-Z](http://www.niddk.nih.gov/health-information/health-topics/Anatomy/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Diabetes A-Z](http://www.niddk.nih.gov/health-information/health-topics/diabetes/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Diagnostic Tests A-Z](http://www.niddk.nih.gov/health-information/health-topics/diagnostic-tests/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Diet & Nutrition A-Z](http://www.niddk.nih.gov/health-information/health-topics/diet/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Digestive Diseases A-Z](http://www.niddk.nih.gov/health-information/health-topics/digestive-diseases/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Endocrine & Metabolic Diseases A-Z](http://www.niddk.nih.gov/health-information/health-topics/endocrine/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Kidney Disease A-Z](http://www.niddk.nih.gov/health-information/health-topics/kidney-disease/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Liver Disease A-Z](http://www.niddk.nih.gov/health-information/health-topics/liver-disease/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Urologic Diseases A-Z](http://www.niddk.nih.gov/health-information/health-topics/urologic-disease/Pages/default.aspx)</span></span>
    -   <span style="background-color: transparent;"><span style="background-color: transparent;">[Glossary of Terms Related to Healthy Eating, Obesity, Physical Activity, and Weight Control](http://win.niddk.nih.gov/publications/glossary.htm)</span></span>
-   <span style="background-color: transparent;">NIH </span><span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Institute of General Medical Sciences (NIGMS)</span>
    -   [<span style="background-color: transparent;"><span style="background-color: transparent;">Inside the Cell Glossary</span></span>](http://publications.nigms.nih.gov/insidethecell/glossary.html)
    -   [Medicines by Design Glossary](https://publications.nigms.nih.gov/medbydesign/glossary.html)
-   NIH <span style="line-height: 20.3999996185303px;">– National Institute of Mental Health (NIMH)</span>
    -   <span style="line-height: 20.3999996185303px;">[Brain Basics Glossary](http://www.nimh.nih.gov/health/educational-resources/brain-basics/brain-basics.shtml)</span>
    -   <span style="line-height: 20.4px; background-color: transparent;">[Publications](http://www.nimh.nih.gov/health/publications/index.shtml)</span>
    -   [What Is Prevalence?](https://www.nimh.nih.gov/health/statistics/prevalence/index.shtml)
-   NIH <span style="line-height: 20.3999996185303px;">– National Institute of Neurological Disorders and Stroke (NINDS)</span>
    -   <span style="line-height: 20.3999996185303px;">[Disorders A-](http://www.ninds.nih.gov/disorders/disorder_index.htm)</span><span style="line-height: 20.3999996185303px;">[Z](http://www.ninds.nih.gov/disorders/disorder_index.htm)</span>
    -   [The Life and Death of a Neuron](http://www.ninds.nih.gov/disorders/brain_basics/ninds_neuron.htm)
-   <span style="line-height: 20.3999996185303px; background-color: transparent;">NIH </span><span style="line-height: 20.3999996185303px; background-color: transparent;">– National Institute on Aging (NIA)</span>
    -   <span style="line-height: 20.3999996185303px;">[Biology of Aging Glossary](http://www.nia.nih.gov/health/publication/biology-aging/glossary)</span>
    -   [Understanding Memory Loss Glossary](https://www.nia.nih.gov/alzheimers/publication/understanding-memory-loss/words-know)
-   <span style="line-height: 20.4px; background-color: transparent;">NIH </span><span style="line-height: 20.4px; background-color: transparent;">– National Institute on Aging (NIA) and National Institute of Neurological Disorders and Stroke (NINDS) </span><span style="line-height: 20.4px;">– [The Dementias: Hope Through Research Glossary](https://www.nia.nih.gov/alzheimers/publication/dementias/glossary)</span>
-   <span style="background-color: transparent;">NIH </span><span style="background-color: transparent;">– National Institute on Aging (NIA) and National Library of Medicine (NLM) </span>– [NIHSeniorHealth](http://nihseniorhealth.gov/)
-   <span style="line-height: 20.3999996185303px; background-color: transparent;">NIH </span><span style="line-height: 20.3999996185303px; background-color: transparent;">– National Institute on Deafness and Other Communication Disorders (NIDCD) </span><span style="line-height: 20.3999996185303px;">– [Health Information Glossary](http://www.nidcd.nih.gov/health/glossary/pages/glossary.aspx)</span>
-   <span style="background-color: transparent;">NIH </span><span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Library of Medicine (NLM)</span>
    -   <span style="background-color: transparent;">[<span style="background-color: transparent;">Medical Subject Headings (MeSH)</span>](http://www.ncbi.nlm.nih.gov/mesh/)</span>
    -   [What Is a Cell?](https://ghr.nlm.nih.gov/primer/basics/cell)
-   NIH <span style="background-color: transparent;">– </span><span style="background-color: transparent;">National Library of Medicine (NLM) and National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK) <span style="background-color: transparent;">– </span>[<span style="background-color: transparent;">LiverTox</span>](https://livertox.nlm.nih.gov/glossary.html)</span>
-   NIH – Osteoporosis and Related Bone Diseases: National Resource Center – [Osteoporosis and Related Bone Diseases Glossary](http://www.niams.nih.gov/Health_Info/Bone/About_Us/Glossary.asp)
-   <span style="background-color: transparent;">Wikipedia and Wiktionary </span>– [Bibliographic Citations for Definitions from Wikipedia and Wiktionary](http://www.ncbi.nlm.nih.gov/pubmedhealth/about/wikipedia-wiktionary-references/)

[Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/), an evidence-based analysis of health stories in the news, is from England's National Health Service [(NHS Choices)](http://www.nhs.uk). (More about [Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/about/).)

We also use [Micromedex Consumer Medication Information](http://www.micromedex.com/). 

PubMed Health is updated when our partners provide new or updated information.

Please see the [Help](http://www.ncbi.nlm.nih.gov/pubmedhealth/help/) page for guidance on searching PubMed Health and citing PubMed Health pages.

### **Contact us**

To send suggestions, recommendations for new resources and/or updates to existing information, e-mail us at <pmh-help@ncbi.nlm.nih.gov>.

Specific medical advice will not be provided. NCBI and NLM urge you to consult with a qualified physician for diagnosis and for answers to your personal questions.

[How do we know what works](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)

[How to read health news](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/how-to-read/)


