![Arrow hitting target](/core/assets/pmh/images/PMH-arrow-target.jpg)

Systematic Review Methods Filter at PubMed
==========================================

Systematic reviewing is the formal study of results from multiple studies. It uses systematic methods to find and analyze relevant studies. Meta-analysis, a statistical technique, is often used to combine results.

The PubMed systematic review methods filter finds publications to support this process. They could relate to the development or evaluation of any step in doing or using systematic reviews.

### The type of publications included

The scope covers both methods research and guidance. Articles that are solely educational or opinion pieces are not included. The publications include citations to journal articles as well as full-text documents at PubMed.

Methods research studies include:

-   comparative evaluations of techniques
-   development, evaluation, or validation of a technique
-   analyses of the methods used by studies
-   consensus and Delphi studies and surveys of methods

Systematic reviews of these studies are also included.

The methods filter is <span>a result of collaboration between the PubMed Health team and the Scientific Resource Center ([SRC](http://www.epc-src.org/)). </span><span>The SRC team selects the publications, after scanning widely every day looking for new candidates. The SRC is funded by </span><span>the U.S. Agency for Healthcare Research and Quality (AH</span><span>RQ) for the [Effective Healthcare Program](http://www.effectivehealthcare.ahrq.gov)</span><span>.</span>

### How to use the PubMed filter

Use this search string in [PubMed's search box](http://www.ncbi.nlm.nih.gov/pubmed/?term=sysrev_methods%5Bsb%5D):

sysrev\_methods [sb]

You can combine it with other search terms, [for example](http://www.ncbi.nlm.nih.gov/pubmed/?term=sysrev_methods+%5Bsb%5D+AND+%22meta-analysis%22):

sysrev\_methods [sb] AND meta-analysis

You can use the filter to get email alerts for added publications from PubMed if you have a [My NCBI account](http://www.ncbi.nlm.nih.gov/myncbi/). <span>The service is free, and PubMed will not send you emails you have not requested. You can unsubscribe directly from an email as well.</span>

The added publications are not necessarily new. The alert will include any publications that are new to either PubMed or to the filter. A backlog of publications will be added going into the first few months of 2016.

To set up an alert, do the search for which you would like to receive updates. Then click on the “Create alert” link below the search box. Make sure to choose whether you would like your alerts to arrive every day, weekly, or once a month.

<span>If you would like to get an email alert when a Cochrane review is updated, you can set that up [at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/05/update-alerts-Cochrane-NCI/).</span>

If you would like to have this filter showing results as a standard part of PubMed, you can do that with a [My NCBI account](http://www.ncbi.nlm.nih.gov/myncbi/), too. <span>The results will then show whenever you use PubMed while signed into your account.</span>

Here is how to set that up:

1.  Go to [My NCBI filters](http://www.ncbi.nlm.nih.gov/myncbi/filters/). (You can get there by clicking on “NCBI Site Preferences” at the top right of your My NCBI account. Then choose “Filters & Icons” under “PubMed Preferences”.)
2.  Click on the blue “Create custom filter” box.
3.  Copy this into the “Query terms” box: sysrev\_methods [sb]
4.  Press “Test This Query” to make sure it worked.
5.  Add a name (like Sysrev Methods) to the “Save filter as” box. That name will appear on your PubMed screen later.
6.  Press “Save Filter”.

The search results will now show at the top right of all your PubMed searches. You can remove this filter or change the name by clicking on the “Manage Filters” link that appears below those results.

### Effectiveness research methods resources at PubMed Health

PubMed Health’s [methods resources](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/06/research-methods/) have the same inclusion criteria as the PubMed filter, with one exception.

<span>PubMed Health partners sometimes do methods research that is not specifically about systematic reviewing. For example, there are Cochrane methods reviews related to clinical trials (like [informed consent methods for trials](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012260/)). These are all included at PubMed Health, but not in the PubMed filter.</span>

Searches at PubMed Health include a simultaneous search of PubMed using the PubMed systematic review methods filter. Those PubMed results are shown in a section to the right of the PubMed Health results.

PubMed Health also includes some pages providing explanations of research methods, including [meta-analyses](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025748/) and [systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025759/). 

*The PubMed Health Team*
 *8 December 2015*


