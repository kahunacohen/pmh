![Image of doctor and patient.](/core/assets/pmh/images/PMH_patient.jpg)

Approved drug uses
==================

Doctors in the USA can prescribe any drug that has been approved by the Food and Drug Administration (FDA).

The FDA approves each drug for one or more indications. An indication is a particular use for the drug, such as treating asthma. A group of people can also be specified, for example, adults.

The FDA-approved indications are included in the information that comes with the drug. That information is called the drug label or structured product label.

You can check the FDA-approved indications for a drug by:

-   reading the leaflet that comes with the drug
-   searching in the [DailyMed](http://dailymed.nlm.nih.gov/dailymed/about.cfm?CFID=78051376&CFTOKEN=79557a774bd8aa7e-3D1CE941-E58C-F486-F74D21676943E590&jsessionid=84301de7f02b1bc621c1613d6f78796a5924%20) website
-   asking your doctor or pharmacist

Other uses
----------

It is very common for a drug to be prescribed for other indications, for other groups of people, or in a different dosage than shown in the label information. This is called off-label prescribing.

An off-label use of a drug can be both well-studied and standard medical practice. However, sometimes a drug has not been tested as well for an off-label use. Insurance companies may not always cover the costs of off-label use.

A drug may be approved for different indications by a regulatory agency in another country, but not by the FDA. So information from different countries may mention "approved" or "licensed" indications that do not apply in the USA.

We include information throughout the website that is based on research into approved and off-label indications.

*By PubMed Health, 20 August 2015.*

[Drug names and classes](/pubmedhealth/drug-names-and-classes/)

[Finding information about a drug](/pubmedhealth/finding-drug-information/)

[Tips about using medicines](/pubmedhealth/using-medicines/)


