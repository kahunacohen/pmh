![description of image](/core/assets/pmh/images/PMH_arrow.jpg)

Keeping up at PubMed Health
===========================

You can keep up with PubMed Health information, tips and more at our [blog](/pubmedhealth/blog/).

Learn how to refine your PubMed Health searches, including by time period [here](/pubmedhealth/blog/2013/07/refine-search-results-PubMedHealth-faceted-search/).

Find out about systematic reviews and evidence at PubMed and PubMed Health [here](/pubmedhealth/finding-systematic-reviews/).

You can use My NCBI to get email alerts when a Cochrane review or National Cancer Institute evidence-based information is updated. Find out about this and other alerts for PubMed Health [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/05/update-alerts-Cochrane-NCI/). 

Find documents about the best research and statistical techniques for systematic reviews and effectiveness research in our Methods Resources collection and on PubMed. Learn about it [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/researchers/).

### All clinical effectiveness information added or updated:          

### [In the last week](/pubmedhealth/?term=pmh_sr_plus_week%5Bsb%5D)               [In the last month](/pubmedhealth/?term=pmh_sr_plus_month%5Bsb%5D)

Systematic reviews of evidence in the last month
------------------------------------------------

-   [All systematic reviews](/pubmedhealth/?term=pmh_sr_month%5Bsb%5D)
-   [Cochrane Collaboration only](/pubmedhealth/?term=pmh_cc_month%5Bsb%5D)
-   [Other systematic reviews only](/pubmedhealth/?term=pmh_sr_other_month%5Bsb%5D)

Evidence-based information in the last month
--------------------------------------------

-   [For consumers](/pubmedhealth/?term=pmh_cons_month%5Bsb%5D)
-   [For clinicians](/pubmedhealth/?term=pmh_clin_month%5Bsb%5D)<span>    </span>


