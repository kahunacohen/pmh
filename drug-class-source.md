Drug class
----------

These drug classes include the U.S. Food and Drug Administration (FDA) [Established Pharmacologic Classes (EPCs)](http://www.fda.gov/ForIndustry/DataStandards/StructuredProductLabeling/ucm162549.htm%20).

Other abbreviations, synonyms, or groups are added by PubMed Health.

You can read more about drug classes here.
  
  


