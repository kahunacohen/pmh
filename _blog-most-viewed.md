The list of links to posts, below (but commented-out in the source), will appear in the Discovery column on the Blog pages:

-   [New Research Methods Resources - Plus a PubMed Filter](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/12/PubMed-filter-for-systematic-review-methods/)
-   [Things You Need To Know About Drug Classes](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/09/things-to-know-about-drug-classes/)
-   [PubMed Health Expands to Research on Research Methods](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/06/research-methods/)


