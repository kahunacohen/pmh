![Photo of man chopping tomatoes](/core/assets/pmh/images/Cooking-with-tomatoes.jpg)

Tomatoes are in the news again. That’s the main source of lycopene in Americans’ diets. Lycopene is an antioxidant – it’s what makes tomatoes red.

Results have just been reported from [a study](http://www.ncbi.nlm.nih.gov/pubmed/25017249) looking at whether men with prostate cancer reported eating less tomatoes than men who did not have prostate cancer.

It’s “biologically plausible” that eating enough tomatoes could help prevent cancer, according to the NHS [Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/) team that analyzed the study's results. “Tomatoes are a rich source of lycopene, a nutrient thought to protect against cell damage. However, the jury is still out on whether it really does protect cells.”

The new study is not a randomized controlled trial. The National Cancer Institute's (NCI) [summary of the evidence](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0032837/%20) on prostate cancer prevention points to a trial where lycopene did not reduce prostate cancer risk. The NCI also reports that lycopene supplements have not been shown to work.

The NCI’s advice is echoed by the Behind the Headlines team’s final take: “So a healthy, balanced diet, regular exercise and stopping smoking are still the way to go. It’s unlikely that focusing on one particular food will improve your health.”

Particularly interested in tomatoes and getting more lycopene, though? The NCI points out that lycopene is best absorbed when the tomatoes have been cooked, accompanied by fat or oil. 

*The PubMed Health Team*

[Featured at Behind the Headlines on PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/)

[The National Cancer Institute (NCI) summary for the community about prostate cancer prevention](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0032540/)

[The NCI report on evidence and prostate cancer prevention](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0032837/)

[Read more about randomized controlled trials in *Testing Treatments*](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0050892/)


