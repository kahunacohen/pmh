![Pharmacist talking to mother and child](/core/assets/pmh/images/Blog-drug-classes-2015-09.jpg)

*“Don’t take this drug if you take a calcium channel blocker.”*

That’s good to know – drug interactions can reduce a drug’s effectiveness or even cause harm. But what’s a calcium channel blocker?

[Calcium channel blocker](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025449/) is the name of a drug class. The drugs block the entry of calcium into the muscle cells in the heart and blood vessels. That can relax the blood vessels and lower blood pressure. So calcium channel blockers are also anti-hypertensives – a drug for managing hypertension (high blood pressure). They can be used for other cardiovascular conditions too, like angina.

A drug can [belong to a drug class](http://www.ncbi.nlm.nih.gov/pubmedhealth/drug-names-and-classes/) because it:

-   acts in the same way – like blocking calcium,
-   is used for the same purpose – like anti-hypertensives, or
-   <span>is related to other drugs by their chemical structure.</span>

<span>Drugs in the same class have something in common. But being in the same drug class doesn’t mean the drugs are the same.</span>

PubMed Health’s [drug information pages](http://www.ncbi.nlm.nih.gov/pubmedhealth/topics/drugs/a/) include some notes about drug class. We are now adding drug classes to the PubMed Health glossary. Along with definitions, we will gradually be adding other classes that drug class is related to, as well as links to the drugs in that class with information pages at PubMed Health. Some examples:

-   [Calcium channel blockers](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025449/) and other [anti-hypertensives](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025447/),
-   [Skeletal muscle relaxants](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025444/), and
-   [HIV medications](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025442/).

Another value to knowing about drug classes of medicines: researchers often analyze information on entire drug classes, or discuss the implications of research findings for people taking any drug in a class. So adding drug classes to the PubMed Health glossary should come in handy when you’re reading research results, too.

There are just a few drug class entries in the PubMed Health glossary so far, but with more than 2,200 drug information pages, there are many more to come! We hope you find the pages useful when you’re looking for information about medications.

*The PubMed Health Team*

[About drug names and classes](http://www.ncbi.nlm.nih.gov/pubmedhealth/drug-names-and-classes/)

[More on the PubMed Health glossary](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2014/04/information-support-tools/)


