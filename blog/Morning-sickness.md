![Photo of pregnant woman with cup of tea](/core/assets/pmh/images/Morning-sickness-2014.jpg)

Nausea and vomiting in pregnancy could sometimes be a good sign, according to a [new systematic review](http://www.ncbi.nlm.nih.gov/pubmed/24893173%20) assessed by the NHS [Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/) team.

Researchers from Canada's [Motherisk program](http://www.motherisk.org/women/index.jsp%20) were able to identify 10 studies that analyzed pregnancy outcomes related with nausea and vomiting in pregnancy.

The researchers found some signs that it can be associated with some good outcomes – even when women have used treatments that were able to reduce their symptoms. For example, two studies involving over 3,200 pregnant women found they had a lower risk of miscarriage.

The NHS team emphasize that this study points to a positive side of morning sickness, but: “Importantly, it does not prove the opposite – that a pregnancy without nausea and vomiting means a poorer outcome.”

Nausea and vomiting is very common in pregnancy – and unfortunately there is not much proven to help. However, there may be a pay-off – even if it is no guarantee of a healthy pregnancy.

*The PubMed Health Team*

[The analysis from Behind the Headlines on PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2014-07-30-morning-sickness-linked-to-healthier-babies/)

[Read about what’s been tested in trials for milder nausea and vomiting in early pregnancy](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0014524/%20)

Early pregnancy is roughly the first trimester: [more about the length of the trimesters of pregnancy](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0023078/%20)


