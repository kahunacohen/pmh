![Image of Wikipedians.](/core/assets/pmh/images/PMH_Wikipedians.jpg)

PubMed Health’s collaboration with Wikipedia took an exciting leap forward when WikiProject Medicine came to visit at the end of May. [WikiProject Medicine](http://en.wikipedia.org/wiki/Wikipedia:WikiProject_Medicine%20)[](#_ftn1) is where people gather to collaborate on medicine and health in Wikipedia.

"Wikipedia really is a miracle," said David Lipman, Director of the National Center for Biotechnology Information ([NCBI](http://www.ncbi.nlm.nih.gov/%20))[](#_ftn2), in his welcoming remarks. "Almost any topic - I go to Wikipedia, and I find something useful. At the very least, it’s always the beginning of finding useful information."

That’s why Wikipedia is now the [most used](http://en.wikipedia.org/wiki/List_of_most_popular_websites%20)[](#_ftn3) reference website in the world. There are over [20 billion visits](http://blog.wikimedia.org/2013/04/19/wikimedia-projects-500-million/)[](#_ftn4) to Wikipedia pages in a month – and [over 200 million](http://en.wikipedia.org/wiki/Wikipedia:WikiProject_Medicine/Popular_pages%20)[](#_ftn5) of those are to pages related to medicine or health.

When the National Library of Medicine’s NCBI began developing PubMed Health to help people [find and use systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2013/04/keep-up-with-evidence/),[](#_ftn6) Wikipedia was a logical [port of call](http://wikimania2012.wikimedia.org/wiki/Submissions/US_National_Library_of_Medicine’s_PubMed_Health:_systematic_reviews_as_a_resource_for_Wikipedia_medical_pages%20)[](#_ftn7).

Systematic reviews aim to minimize bias – and "[NPOV](http://en.wikipedia.org/wiki/Wikipedia:Neutral_point_of_view%20)"[](#_ftn1) (a neutral point of view) is central to Wikipedia. Wikipedia designates systematic reviews as ["ideal" reliable sources](http://en.wikipedia.org/wiki/Wikipedia:Identifying_reliable_sources_(medicine)%20)[](#_ftn2) for its medical pages.

What’s more, Wikipedia called on the medical community to [join in editing](http://www.ncbi.nlm.nih.gov/pubmed/21282098)[Wikipedia](http://www.ncbi.nlm.nih.gov/pubmed/21282098)[](#_ftn1) – and the NIH houses a formidable [pool of expertise](http://www.nih.gov/about/%20)[](#_ftn2) along with the U.S. National Library of Medicine[](#_ftn3) ([NLM](http://www.nlm.nih.gov/%20)). In 2009, the NIH had organized a [Wikipedia Academy](http://nihrecord.od.nih.gov/newsletters/2009/09_04_2009/story2.htm%20)[](#_ftn4) with the [Wikimedia Foundation](http://wikimediafoundation.org/wiki/Home%20),[](#_ftn5) establishing guidelines for participating in Wikipedia from [NIH](http://www.nih.gov/icd/od/ocpl/resources/wikipedia/%20).[](#_ftn6) And the Wikimedia community has since begun an outreach program with cultural institutions called [GLAM](http://outreach.wikimedia.org/wiki/GLAM%20) [](#_ftn7)(for "galleries, libraries, archives and museums").

![Image of Wikipedia logo at NLM.](/core/assets/pmh/images/PMH_WP_at_NLM.jpg)

### World’s biggest encyclopedia – meet the world’s biggest medical library!

The activities at NLM were [a joint project](http://en.wikipedia.org/wiki/Wikipedia:WikiProject_Medicine/May_2013%20)[](#_ftn1) with WikiProject Medicine. [James Heilman](http://www.who.int/bulletin/volumes/91/1/13-030113.pdf%20)[](#_ftn2) traveled to Bethesda from Canada, and Lane Rasberry[](#_ftn3) traveled from New York. James is an emergency room doctor who’s a Wikipedia [Administrator](http://en.wikipedia.org/wiki/Wikipedia:Administrators%20)[](#_ftn4) (the Wikipedia equivalent of moderators). Lane is [Wikipedian-in-residence](http://wikistrategies.net/cr-on-lane-rasberry/%20)[](#_ftn5) at Consumer Reports.

[Edit-a-thons](http://en.wikipedia.org/wiki/Wikipedia:How_to_run_an_edit-a-thon%20)[](#_ftn6) in a packed training room and invited meetings over three days attracted over 60 people. There were Wikipedians and staff from PubMed Health, the National Library of Medicine[](#_ftn7) and from all around the National Institutes of Health[](#_ftn8) (NIH).

We started on Tuesday 28 May with a tour of the Library organized by NLM Office of Communications and Public Liaison[](#_ftn9) ([OCPL](http://www.nlm.nih.gov/ocpl/ocpl.html%20)) and its [History of Medicine division](http://www.nlm.nih.gov/hmd/%20).[](#_ftn10) Medical information motherlode! You can see photos of Wikipedians enjoying themselves down amongst the closed library stacks underground and more at NLM in the Wikimedia Commons' [photo collection](http://commons.wikimedia.org/wiki/Category:2013_National_Library_of_Medicine_editathon) for the week.

[](#_ftn11)

Plans for future joint activity bubbled up early. Melanie Modlin from OCPL pointed out the oil paintings of NLM’s directors dotting the walls of the Reading Room. Local Wikipedians quickly established there weren’t digital versions online or Wikipedia entries, volunteering to write the articles. NLM OCPL will supply images and pointers to bio information.

By the end of the day, the first Wikipedia entry for an NLM director – current director, [Dr Donald Lindberg](http://en.wikipedia.org/wiki/Donald_A.B._Lindberg%20)[](#_ftn1) – had begun. It was the first of many initiatives of the week, as NIH-ers and Wikipedians worked their way through the role of an institution in the generation of accurate but independent encyclopedia content about it.

![Image of edit-a-thon participants.](/core/assets/pmh/images/PMH_Participants.jpg)

### Wikipedia edit-a-thons at the NLM

What’s a Wikipedia edit-a-thon? Turns out it's a lot of fun! Experienced Wikipedia editors get together with newcomers who are curious and eager to learn the ropes. We packed out a training room twice (28 and 30 May), with more joining by webinar. Each webinar started with talks, including an inspiring and fascinating overview of Wikipedia globally and Wikipedia in medicine by James Heilman and an introduction into how to edit Wikipedia by Lane Rasberry (see below).

Hilda Bastian spoke about PubMed Health’s interest and activities with Wikipedia, focusing on our plans to routinely incorporate Wikipedia editing as part of our process of [featuring reviews](http://feed://www.ncbi.nlm.nih.gov/feed/rss.cgi?ChanKey=pmhfeaturedreviews%20%20) at PubMed Health (see below).[](#_ftn1)

There is a list of the talks and David Lipman’s opening remarks below, along with links to tips on editing Wikipedia and where you can get people to help you with any issues you have when you get started. Learning the ropes of the software can be challenging, but a new visual editor coming in the next few months aims to make it much easier for newcomers.

The edit-a-thons were fun, building relationships and skills. Many took the plunge of their first edit. A bonus was improvement to more than a dozen Wikipedia pages, at least two new Wikipedia entries started and a lot of discussion about ways that NIH Institutes can engage in improving information about themselves without compromising either [NIH or Wikipedia norms](http://www.nih.gov/icd/od/ocpl/resources/wikipedia/).[](#_ftn2)

A meeting on 29 May at NIH’s Wilson Hall organized by Marin Allen, [NIH Office of Communications and Public Liaison](http://www.nih.gov/icd/od/ocpl/%20)[](#_ftn3), drew a wide range of NIH Institutes interested in ongoing collaboration. Presentations by James Heilman, Lane Rasberry and Kristin Anderson from our local chapter, [Wikimedia DC](http://wikimediadc.org/wiki/Home%20),[](#_ftn4) explored next steps in building on the NIH relationship with Wikipedia.

Our thanks to the many people at WikiProject Medicine, Wikimedia DC, NCBI, NLM and NIH OCPL who made the week such a success. We'll keep you posted as our planned activities bear fruit.

*The PubMed Health Team*

**Would you like to be kept informed of future Wikipedia activities at NLM and NIH?** Then email <pmhmeet@nih.gov> or add your name [at Wikipedia](http://en.wikipedia.org/wiki/Wikipedia:WikiProject_Medicine/May_2013).

Read about how to [edit Wikipedia medical pages](http://en.wikipedia.org/wiki/Wikipedia:MEDHOW%20)

Or start with a more [general introduction on editing Wikipedia](http://outreach.wikimedia.org/wiki/File:Welcome_to_Wikipedia_brochure_EN.pdf)

Get editing help online from the friendly people at [Wikipedia Teahouse](https://en.wikipedia.org/wiki/Wikipedia:Teahouse)

Evaluation of Wikipedia content [on PubMed](http://1.usa.gov/11DrlXl)

[](#_ftn1)

View recorded talks from webinar, including captions and slides:

-   [Dr David Lipman (NCBI) and Dr James Heilman (WikiProject Medicine)](https://webmeeting.nih.gov/p83545959/) [40 min]

Slides from the Edit-a-thons:

-   James Heilman – [Wikipedia and Medicine](http://bit.ly/1aAh0Cf)
-   Hilda Bastian – [PubMed Health: Finding & choosing info online or at the NLM](http://bit.ly/12S7usy)

**The Wikipedia logo is a trademark of the Wikimedia Foundation, used here with permission.**

*Photos:*

*At top:* Kristin Anderson, James Heilman and Lane Rasberry: Wilson Hall, NIH on Wednesday, 29 May 2013.

*Below:* The first WikiProject Medicine/NLM edit-a-thon in NLM’s Lister Hill Center on Tuesday, 28 May 2013. From left: Mary Mark Ockerbloom, James Heilman, Janice Rice Okita.

There are more photos in [Wikimedia Commons](http://commons.wikimedia.org/wiki/Category:2013_National_Library_of_Medicine_editathon%20). 


