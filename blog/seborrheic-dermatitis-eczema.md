![Teenage boy with right hand covering part of face](/core/assets/pmh/images/Dandruff-2014-06.jpg)

What do dandruff and baby’s cradle cap have in common? They’re both caused by a condition called seborrheic dermatitis (SD) or seborrheic eczema. For babies, a mild case is normal – about 70% of babies get it. It goes away on its own quite quickly. According to UK’s [National Institute for Health and Care Excellence](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0015551/) (NICE), you only need water to take care of your newborn’s skin.

For teenagers and adults, though, SD can be more severe, and affect more than just the scalp. It can appear anywhere with lots of [sebaceous (oil) glands](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022676/%20), including the face.

According to a new [Cochrane systematic review](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0065325/)\* by researchers from the national medicines agency in Finland, SD comes and goes in adolescents and adults. It’s not clear what causes it.

SD is more common in men, doesn’t cause baldness – and increases after the age of 50. And it’s more common in people with some diseases, including Parkinson disease and HIV.

The researchers found trials studying topical anti-inflammatories for the scalp and face in just over 2,700 people.

Topical treatments are applied to the skin, including shampoos and gels. Anti-inflammatories are drugs that aim to reduce the irritation to the skin SD causes.

These treatments can help SD on the scalp and face in the short-term. But some ingredients cause more adverse effects than others. Some lower dose treatments may be just as effective as higher dose ones.

Read more in the [researchers’ summary for consumers](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0065325/) at PubMed Health.

*The PubMed Health Team*

**More information from PubMed Health**

[About the skin](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022679/)

[About seborrheic dermatitis, including links to more systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022916/)

[About systematic reviews of evidence](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

 

\* Kastarinen H, Oksanen T, Okokon EO, Kiviniemi VV, Airola K, Jyrkkä J, Oravilahti T, Rannanheimo PK, Verbeek JH. Topical anti‐inflammatory agents for seborrhoeic dermatitis of the face or scalp. *Cochrane Database of Systematic Reviews* 2014, Issue 5. [[Free summary at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0065325/)] [[Full text at The Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD009446.pub2/full)]


