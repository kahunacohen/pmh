![Woman reading](/core/assets/pmh/images/Woman-reading-2013-07.jpg)

There have been lots of headlines recently, saying a study showed reading could protect against Alzheimer’s. According to these reports, if your brain is well-exercised, you might not experience cognitive decline so quickly – perhaps not even when your brain is affected.

Very welcome tidings – but could that study really show that? If you feel like reading something, see [what the team from Behind the News said](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/) about this.

You could also catch up on some [evidence-based information from PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005104/%20%20) on trials studying brain training.

*The PubMed Health Team*

**Also from NHS:** [How to read health news](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/how-to-read/)

**Links to more recent NHS "Behind the Headlines" analysis:**

[Exercise in a pill is still the stuff of fiction](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2013-07-18-exercise-in-a-pill-is-still-the-stuff-of-science-fiction/%20)

[Smart knife can tell cancer cells from healthy tissue](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2013-07-18-smart-knife-can-tell-cancer-cells-from-healthy-tissue/)


