![Systematic review methods](/core/assets/pmh/images/PMH-blog-methods-filter-2015.jpg)

If you’re interested in the scientific methods behind systematic reviews, we’ve now made it easier for you to find publications about the process.

Earlier this year we expanded PubMed Health to [research on research methods](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/06/research-methods/). These are studies and guidance for doing systematic reviews and helping them make an impact.

We have taken this a step further with a new resource – the PubMed Systematic Review Methods Filter – and a new section at PubMed Health “For Researchers”. More on this in a moment.

We also have an addition for anyone who wants to understand more about the mechanics of health research: new glossary pages especially for research methods.

Now if you see a term like "[random allocation](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025810/)" or "[observational study](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025839/)" with a link, you can click to find out more about what it means. The glossary will grow to cover the most common research terms used inside PubMed Health.

These pages are one of the ways you might come across results from the new methods filter. 

Whenever you search in PubMed Health, you also get results from PubMed using this filter. You’ll see them over to the right of the main search results, in a box called “Systematic Review Methods in PubMed”. (It’s below a box called “Systematic Reviews in PubMed”. That’s a search for systematic reviews themselves.)

So, what does the methods filter do? And how can you use it yourself in PubMed?

![Sysrev\_methods filter](/core/assets/pmh/images/PMH-blog-sysrev_meth.jpg)

The filter searches through a subset of PubMed records that are either research or guidance on systematic review methods. The publications could relate to the development or evaluation of any step in doing or using systematic reviews.

That’s a very wide range. There are over 6,500 publications already and many more to come. They cover statistical and research design issues, as well as techniques for finding and critiquing studies. There is research on topics like how to involve members of the public, and how to communicate research results.

To use the filter in PubMed, enter this [in the search box](http://www.ncbi.nlm.nih.gov/pubmed/?term=sysrev_methods%5Bsb%5D):

sysrev\_methods [sb]

You can use it like any search term. For example, [click to here to see](http://www.ncbi.nlm.nih.gov/pubmed/?term=sysrev_methods%5Bsb%5D+AND+%22network+meta-analysis%22) what you get if you use this search:

sysrev\_methods [sb] AND "network meta-analysis"

You can get e-mail alerts for new additions, or customize your PubMed settings so that the filter is always automatically running and showing you results. You can find out how to do all this on [our detailed page about the filter](http://www.ncbi.nlm.nih.gov/pubmedhealth/researchers/pubmed-systematic-review-methods-filter/).

The methods filter is a result of collaboration between the PubMed Health team and the Scientific Resource Center ([SRC](http://www.epc-src.org)) for the U.S. Agency for Healthcare Research and Quality ([AHRQ](http://www.effectivehealthcare.ahrq.gov)). The SRC team selects the publications, after scanning widely every day looking for new candidates.

The collection of methods resources is also growing steadily at PubMed Health. Citations to those publications are added to PubMed.

When you see a systematic review at PubMed Health, there will often be a methods box to the right. That links to the relevant methods guide from the organization behind the review. Six groups so far have started contributing their methods guidance and research to PubMed Health. You can see the list on our new "[For Researchers](http://www.ncbi.nlm.nih.gov/pubmedhealth/researchers/)" page.

That page is now the “home” for information on resources for researchers. You can find your way to it from anywhere in PubMed Health. Just click on the new “For Researchers” section in the blue navigation bar across the top of each page.

The navigation isn’t the only part of PubMed Health with a new look. Our homepage has a new face too. Happy searching!

*The PubMed Health Team*

[More on research on research methods at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/06/research-methods/)
 [About using filters at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2013/07/refine-search-results-PubMedHealth-faceted-search/)
 [More on the PubMed Health glossary](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2014/04/information-support-tools/)

*The image on this blog post is in the public domain. Created by PubMed Health, using data from [a systematic review on atrial fibrillation](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0078774/) *produced by AHRQ, the U.S. Agency for Healthcare Research and Quality. ([Atrial fibrillation](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0023162/) is a *problem with the rhythm of the heartbeat.)***


