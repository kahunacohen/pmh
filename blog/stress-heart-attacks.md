![Doctors and nurses running down a hallway](/core/assets/pmh/images/BtH-2014-06-24-blogphoto.jpg)

A possible explanation for the way stress could ultimately lead to heart attacks is in the news – and the NHS Behind the Headlines team has [analyzed the story](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2014-06-23-stress-causes-damage-to-the-heart-study-finds/). 

[The research](http://www.nature.com/nm/journal/vaop/ncurrent/full/nm.3589.html) involves involves 29 medical residents, laboratory mice, and hypotheses about the impact of stress on the development of blood cells.

Valuable knowledge was gained in this laboratory study, the NHS team concluded: “However, there are a lot of maybes.” Their analysis is an interesting look at the contribution – and caveats – of [preclinical research](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/). 

Can you counteract any impact of stress on your heart health? According to a 2012 systematic review from Germany’s Institute for Quality and Efficiency in Health Care (IQWiG), the jury’s still out on stress management and high blood pressure – a key risk factor for a heart attack. Find out more about [this research at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0049867/). 

*The PubMed Health Team*

[Learn the language of blood cells and their development](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022038/)

[Learn about coronary artery disease](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0053067/)

[Featured at Behind the Headlines on PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/)


