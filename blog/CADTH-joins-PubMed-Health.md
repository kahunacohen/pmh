![Image of office workers in conference room.](/core/assets/pmh/images/Blog_CADTH_2013-12-26.jpg)

We are delighted to welcome the Canadian Agency for Drugs and Technologies in Health (CADTH) to PubMed Health. CADTH has been providing systematic reviews and other assessments to support decision-making in the [Canadian healthcare system since 1989](http://www.cadth.ca/en/cadth/history).

Digitized full texts of CADTH systematic reviews are being added progressively to PubMed Health. An example is one on [management of relapsing-remitting multiple sclerosis](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0060855/) from March this year. You can see the full list at any time [using this search](http://www.ncbi.nlm.nih.gov/pubmedhealth/?term=pmh_sr[sb]&filters=cadth).

![Screen shot of faceted search with CADTH.](/core/assets/pmh/images/Blog_CADTH_screen-shot-2013-12-23.jpg)

You can use the faceted search on PubMed Health to restrict your searches to any of our information partners. You choose “More…” to see all the providers and select the one you want (as we did here with CADTH – shown on the left).

We have a [blog post explaining faceted searching](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2013/07/refine-search-results-PubMedHealth-faceted-search/) to help you get the most out of using PubMed Health.

CADTH joins several other health technology assessment (HTA) agencies from the US, England and Germany who are [PubMed Health information partners](http://www.ncbi.nlm.nih.gov/pubmedhealth/about/). HTA agencies provide evidence-based information to guide policy decisions, often undertaking [systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/) of the effects of a new drug or medical device. Decisions on whether or not to publicly fund access to these are often made using these assessments. 

*The PubMed Health team*

You can find out more about CADTH at [their website](http://www.cadth.ca/).


