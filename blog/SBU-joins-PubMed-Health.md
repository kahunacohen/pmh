![SBU logo](/core/assets/pmh/images/SBU-blog.jpg)

We are delighted to welcome the Swedish Council on Health Technology Assessment ([SBU](http://www.sbu.se/en/)) to PubMed Health. The SBU has been providing systematic reviews and other assessments to support decision-making in the Swedish health service [since 1987](http://www.inahta.org/our-members/members/sbu/)—and now they cover social services as well.

Topics already online now include:

<span>•   Interventions to improve the care for </span>[<span>pe</span><span>ople with mental illnesses</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0078716/)<span> from </span><span>family doctors and community nurses;</span>

<span>•   </span><span>The pros and cons of a </span>[<span>test for liver d</span><span>isease</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0078710/)<span> </span><span>that might be an alternative to painful liver biopsy;</span>

<span>•   </span><span>A thorough look at the </span>[<span>ne</span><span>ck, shoulder, and arm problems</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0078715/)<span> people develop in different occupations.</span>

There are 19 English summaries of SBU at PubMed Health now: dozens more will follow soon. To make them available more quickly, we are putting them online with just the PDF to download at first. The summaries will be digitized—and additional valuable data <span>will be added to them as well. We have added them to PubMed too.</span>

You can [browse the SBU collection here now](http://www.ncbi.nlm.nih.gov/pubmedhealth/?term=pmh_sr%5Bsb%5D&filters=sbu). You can also narrow down your searches to SBU or any other PubMed Health partner using the filter options on the left side of PubMed Health. We have a [blog post explaining faceted searching](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2013/07/refine-search-results-PubMedHealth-faceted-search/) to help you get the most out of using PubMed Health.

There are now health technology assessment (HTA) agencies from five countries at PubMed Health. HTA agencies provide evidence-based information to guide policy decisions, often undertaking [systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/) of the effects of a new drug or medical device. Decisions on whether or not to publicly fund access to these are often made using these assessments.

We are looking forward to more of them becoming [PubMed Health information partners](http://www.ncbi.nlm.nih.gov/pubmedhealth/about/) in the coming months.

*The PubMed Health Team*

You can find out more about SBU at [their website](http://www.sbu.se/en/).


