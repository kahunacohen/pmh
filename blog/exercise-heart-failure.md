![Woman on a bicycle](/core/assets/pmh/images/Woman-cycling-2014-05.jpg)

Heart failure reduces the capacity to do things that take a lot of effort. Because the heart isn’t pumping properly, normal activities can be very tiring.

For many people with heart failure, the problem is that the heart isn’t pumping blood out with enough force. (That’s called heart failure with “reduced ejection fraction,” or HFREF.)

For others, the problem is that parts of the heart called the [ventricles](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022261/%20) are not relaxing enough between beats. When that happens, the heart can’t fill up with enough blood. (That’s called heart failure with normal or “preserved” ejection fraction: HFNEF or HFpEF.)

According to the [National Heart, Lung, and Blood Institute](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022300/%20), some people with heart failure have both problems.

It’s no wonder, then, that exercise might be difficult when people develop heart failure.

A [Cochrane systematic review](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011983/)\* has just been updated, with important answers to questions about benefits and safety of exercise for people with heart failure. They found more than 30 trials to analyze, involving around 4,700 people with heart failure. Most of them had HFREF: there was little to go on for people with normal ejection fraction.

These studies confirm that exercise may not only be safe for people with heart failure, it might make daily life a little easier, and reduce how often they need to go to the hospital. It’s possible that in the long term, this could translate into living longer, but longer-term studies would be needed to know for sure.

Read more in the researchers’ [summary for consumers](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011983/) at PubMed Health.

 **More information from PubMed Health:**

About [the heart](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0015637/)

About [heart failure](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022300/), including links to consumer information and systematic reviews

About [systematic reviews of evidence](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

\*  Taylor RS, Sagar VA, Davies EJ, Briscoe S, Coats AJS, Dalal H, Lough F, Rees K, Singh S. Exercise‐based rehabilitation for heart failure. *Cochrane Database of Systematic Reviews* 2014, Issue 4. [[Free summary at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011983/%20)] [[Full text at The Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD003331.pub4/full%20)]


