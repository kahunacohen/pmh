![Image of two female college students.](/core/assets/pmh/images/PMH_roommates_blog.jpg)

A new [NHS Behind the Headlines](/pubmedhealth/behindtheheadlines/) feature at PubMed Health tackles whether or not you can 'catch' depression.

The study that triggered the news coverage is about cognitive vulnerability—how well people can cope and distance themselves from negative moods. Researchers at the University of Notre Dame studied 103 roommate pairs of college freshmen.

The NHS analysis explains the research and what it can and cannot tell us. There is a link to the study—and we include an example of news coverage in the US, too.

And if you would like to know more about what you can do when someone you know might be depressed, there is [information on strategies](/pubmedhealth/PMH0005028/) here.

*The PubMed Health Team*

**Also from NHS:** [How to read the health news](/pubmedhealth/behindtheheadlines/how-to-read/%20)

**Links to more recent NHS "Behind the Headlines" analysis:**

-   [Does fish in Mediterranean diets combat memory loss?](/pubmedhealth/behindtheheadlines/news/2013-04-30-does-fish-in-mediterranean-diet-combat-memory-loss/%20)
-   [Have taller women evolved to have more babies?](/pubmedhealth/behindtheheadlines/news/2013-04-29-have-taller-women-evolved-to-have-more-babies/%20)


