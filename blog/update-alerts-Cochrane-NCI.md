![Meerkat standing and looking at something](/core/assets/pmh/images/Meerkat-for-blog-2015-05-08.jpg)

 <span>Ever wished you could get an email alert when a particular Cochrane review or National Cancer Institute (NCI) evidence-based information is updated?</span>

 <span>Now you can at PubMed Health!</span>

 First you need to sign in to your "My NCBI" account. If you don't have one, you need to set one up. It’s free and reliable.

 You can read more about how to do this in "[My NCBI Help](http://www.ncbi.nlm.nih.gov/books/NBK3842/)" or watch our [short video on YouTube](https://www.youtube.com/watch?v=ks46w3mNAQE) (less than 3 minutes).

 ![Signing in to My NCBI](/core/assets/pmh/images/Sign-in-alert.jpg)

The link to My NCBI is at the top right of every PubMed Health page.

 Say you were interested in this [Cochrane review on treatments for chronic ankle instability](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012553/).

 Look to the right for the link, “Get e-mail alert with My NCBI when this is updated.”

 Click and My NCBI will fill in the specific page numbered PMH0012553 as a saved search in your account.

 That ID number is all that My NCBI needs to know about the update you want. In PubMed Health, this will only work for a single ID.

![Screenshot of saved search](/core/assets/pmh/images/PMHID-saved.jpg)

 It looks like the image here on the left.

 Below that, it will show you the e-mail address for your My NCBI account. That’s where the alert will be sent.

 You can also choose a schedule.

 ![Screenshot of schedule dropdown](/core/assets/pmh/images/Frequency.jpg)

Tip: The default setting for a My NCBI alert is monthly. 

 To be sure you get your alert as soon as the page is updated, use the drop down menu in “Frequency” to change to “Daily”.

 <span>You can add a message to yourself if you like – or just press “Save” and it’s done! You can go back in and change it any time. Every e-mail alert from My NCBI offers you links to change or cancel the alert, too.</span>

 <span>If you are interested in evidence about cancer prevention or treatment, you can sign up the same way now for alerts on NCI updates for evidence reviews with patient information like this one on [breast cancer treatment](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0032676/). </span><span>Teams of cancer experts are </span><span>keeping up-to-date with trials and systematic reviews. For a cancer with a lot of research happening, you can expect several updates a year.</span>

 <span>Where else can you use My NCBI to get an alert? The service is available for every search in PubMed Health: click on the “Save search” link with the envelope symbol. And there is link to “Get e-mail alerts with My NCBI” to get systematic reviews from [Health A-Z pages](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2014/04/information-support-tools/) like this one on [Chronic Ankle Instability](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0024935/).</span>

 Want to get updates on all the evidence-based information coming into PubMed Health? Then check out the prepared searches via our “Keeping up with PubMed Health” page and stay informed.

 *The PubMed Health Team*

More about:

[<span>Finding systematic reviews at PubMed Health and in PubMed</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/finding-systematic-reviews/)

[Systematic reviews and clinical effectiveness research](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)


