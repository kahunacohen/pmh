![Image of woman at laptop.](/core/assets/pmh/images/PMH_newfacets.jpg)

There are tens of thousands of articles in PubMed Health now. So if you search with a term like **asthma** you can get a lot of results. There will be information for consumers, for clinicians and systematic reviews. And asthma will get a mention in some educational resources about research, too.

Too much? Not what you expected? Now if you don’t see what you’re looking for straight away, you can narrow down what we show you using faceted search. A faceted search lets you choose one or more filters to progressively reduce the type of articles or books we show you.

The choices you have will appear on the screen to the left of your search results. When you click on one, the results will automatically reduce to just the type of information you have selected.

![Screenshot of facets.](/core/assets/pmh/images/PMH_facet_screenshot.jpg)

Did you only want to see systematic reviews? Then click on that option. A tick and bold text will appear to show you how you’ve narrowed your search down – just like the picture on the left. Your search results will drop to show only those.

Still too many? You can narrow it down further – for example, by clicking on "added in the last 30 days" too.

At each point, the options on the left might drop, as the information still available is refined by your choices. You might suddenly find instead of too much information, you’re getting too little! Uncheck a filter by re-clicking it, use the “clear” for that group – or click on “clear all” to have a fresh start.

There are four groups of options available:

-   Article types – consumer information, systematic reviews, clinical information and educational resources,
-   Recent additions – you can choose a time period or specific dates,
-   Additional filters – systematic reviews that come with a quality assessment, or ones from DARE (the Database of Reviews of Effects),
-   Content providers – our information partners, like the Agency for Healthcare Research and Quality (AHRQ), the Cochrane Collaboration and the National Cancer Institute (NCI).

Some tips to keep in mind: any filters you choose will "stick" until you clear them. Even if you leave PubMed Health and come back soon afterwards, your searches will still be narrowed down that way. A note will be there to remind you.

The facets won’t appear until after you have run a search – and they will disappear if you empty the search box. If you would like to browse through everything on a type of article without limiting the subject, [click here for custom PubMed Health filters](http://www.ncbi.nlm.nih.gov/pubmedhealth/finding-systematic-reviews/).

We hope you find what you’re looking for!

*The PubMed Health Team*

All systematic reviews added to PubMed Health [in the last week](http://www.ncbi.nlm.nih.gov/pubmedhealth/?term=pmh_sr_week%5Bsb%5D%20)

All systematic reviews added to PubMed Health [in the last month](http://www.ncbi.nlm.nih.gov/pubmedhealth/?term=pmh_sr_month%5Bsb%5D%20) (678 at blog time)

More about [PubMed Health filters](http://www.ncbi.nlm.nih.gov/pubmedhealth/finding-systematic-reviews/).


