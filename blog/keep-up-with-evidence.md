![Large green arrow](/core/assets/pmh/images/PMH_large-arrow.jpg)

Welcome to our blog on health, treatments and finding out what works!

We’re a team at the world's largest medical library, the U.S. National Library of Medicine\*. [PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/) specializes in research and easy-to-understand information on the effects of prevention and treatments.

[PubMed](http://www.ncbi.nlm.nih.gov/pubmed/) is a resource for everyone to find health research. There are abstracts—short technical summaries—of more than 22 million scientific articles in biology, medicine and health.

[To find out what works](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/) there has to be a solid understanding of the state of scientific knowledge on a topic. That's done with a systematic review of suitable scientific evidence. Researchers sift through the vast mass of health research to find and analyze the studies that can answer a particular question.

New or updated [systematic reviews get added to PubMed Health](http://www.nlm.nih.gov/pubs/techbull/so11/so11_pm_health.html) every week. We now have about 25,000 of them from the last decade. And there are lots of updates and new information for consumers based on these systematic reviews, too.

We have a new page for seeing [What's New](/pubmedhealth/new/). And we'll help you keep on top of all the new and updated material here at the Blog—along with tips about how to get the most out of PubMed Health.

We also include resources at PubMed Health that aim to help people understand health and health research, and we'll be picking out interesting things from those areas too.

We don’t have a commenting section here, but you can follow us and comment at: [Twitter](https://twitter.com/PubMedHealth), or our new [Facebook page](https://www.facebook.com/PubMedHealth) and [Google+](https://plus.google.com/107599362190099097644/posts).

*The PubMed Health Team*

\* The [National Center for Biotechnology Information](http://www.ncbi.nlm.nih.gov/) (NCBI) creates resources for researchers, particularly large-scale research in human genetics. We also provide public access to information through resources like [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/) and [PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/).

The NCBI is a division of the U.S. [National Library Medicine](http://www.nlm.nih.gov/about/index.html) (NLM) in Bethesda, Maryland. The NLM was founded in 1836, and it's part of the [National Institutes of Health](http://www.nih.gov/) (NIH).


