![Doctor and assistant looking at drug label](/core/assets/pmh/images/Blog-image-timing-medicines.jpg)

Many drugs are aimed at body processes that vary during the day and night – like blood pressure and hormone levels. If genes involved in those processes are more active at certain times of the day, it might influence how well some medicines work taken at that time.

The team from [NHS Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2014-10-28-drugs-may-work-better-at-certain-times-of-the-day/) looked at the study behind recent headlines on this question. Researchers from the [University of Pennsylvania and the University of Missouri studied](http://www.ncbi.nlm.nih.gov/pubmed/25349387) <span>relevant rhythms in gene activity in mice.</span>

The NHS team pointed out that mice have very different [circadian rhythms](http://www.nigms.nih.gov/Education/pages/Factsheet_CircadianRhythms.aspx) to people: they are nocturnal (mostly active at night), while people are diurnal (mostly active during the day). 

The researchers found that many of the genes they studied were affected by rhythms – and there was a “rush hour” in their activity before dawn and before dusk. Many of the most commonly used drugs in the United States are aimed at processes involving these genes.

<span>The NHS Behind the Headlines team pointed out that we need more direct evidence in people to know about the optimal timing to use particular drugs: “Until further evidence is forthcoming, you should follow the advice that comes with your medication in terms of when to take it.”</span>

Our daily rhythms don’t just affect biology though. Routines can have an impact on whether people remember to take medicines regularly, too. Only research in people can weigh up the impact of all the influences.

*The PubMed Health Team*

[Featured at Behind the Headlines on PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2014-10-28-drugs-may-work-better-at-certain-times-of-the-day/%20)

[Information on how to manage medications long-term](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004994/)

[More about circadian rhythms](http://www.nigms.nih.gov/Education/pages/Factsheet_CircadianRhythms.aspx)


