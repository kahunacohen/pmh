![About the Esophagus](/core/assets/pmh/images/esophagus.jpg)

PubMed Health now has new features to help people find, understand, and keep up with clinical effectiveness resources.

The key feature is a glossary that will be used increasingly across our resources. It’s very small now—less than 100 terms have joined the drug names we have had for some time. The terms will include diseases, conditions, treatments, anatomy, biology, and much more.

The PubMed Health team is curating these terms, from dictionaries and glossaries from our information partners and National Institutes of Health (NIH) sources. Some will also be selected from Wikipedia, an outcome of [our collaboration with WikiProject Medicine](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2013/07/Wikipedia-visits-National-Library-of-Medicine-NIH/).

Where it’s relevant, terms are supported by a feed of the latest systematic reviews and consumer resources for that subject. You can sign up for email alerts when there are new systematic reviews or use RSS. This is done through [My NCBI](http://www.ncbi.nlm.nih.gov/books/NBK3842/), a free service from PubMed.

Other data that might be associated with terms—like synonyms and British spellings—are being added. These will also support improved retrieval of results on PubMed Health. You’ll know you have hit on one when a box appears with “About” a search term you used.

![More about Anemia](/core/assets/pmh/images/Anemia_modified.jpg)

From diseases (like [anemia](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0021987/)), to body parts like the [esophagus](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0015635/), or a word like [triglycerides](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022017/), you will encounter a lot of information related to the subject. 

In addition, we’re delighted to welcome a new information partner to PubMed Health: the NIH’s [National Heart, Lung, and Blood Institute (NHLBI)](http://www.nhlbi.nih.gov/). Their excellent consumer articles will support these new pages at PubMed Health.

Expect the collection to grow quickly. We’ll keep you posted—and we hope these tools will improve your experience of using clinical effectiveness information at PubMed Health.

*The PubMed Health Team*

**More about**:

[PubMed Health's fledgling Health A–Z](http://www.ncbi.nlm.nih.gov/pubmedhealth/topics/health/a/)

[PubMed Health's information partners and sources](http://www.ncbi.nlm.nih.gov/pubmedhealth/about/)

[My NCBI](http://www.ncbi.nlm.nih.gov/books/NBK3842/)

 


