![Woman looking at her reflection in a window.](/core/assets/pmh/images/Woman-reflection-2013-08.jpg)

[Migraine headaches](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005091/) can be disabling. Migraines affect about 8% of girls and 17% of adult women in the US. The rate of migraines for boys is about 5% and that stays about the same for adult men.

Migraines are regarded as chronic if they happen frequently – more than 15 days a month, month after month. And that happens to about 2% of children, adolescents and adults.

The medical term for preventing chronic migraines is migraine prophylaxis.

The Agency for Healthcare Research Quality (AHRQ) recently published a systematic review on medications for preventing migraines in [children and adolescents](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0057605/), and another on prophylaxis for [adults](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0056465/). Successful prevention was defined as decreasing the number of migraines by half or more.

[Systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/) are research projects that involve finding and analyzing the results of studies. The AHRQ review team found many trials evaluating drug effectiveness for reducing chronic migraines, although the research generally was not high quality.

No drug is [approved by the Food and Drug Administration (FDA)](http://www.ncbi.nlm.nih.gov/pubmedhealth/approved-drug-uses/) for migraine prophylaxis in children, although four drugs are approved for adults. Read more about which drugs might help in the links below.

*The PubMed Health Team*

**The AHRQ systematic reviews:**

Shamliyan TA, Kane RL, Ramakrishnan R, Taylor FR. [Migraine in children: Preventive pharmacologic treatments](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0057605/). Comparative Effectiveness Reviews No 108. Rockville (MD): Agency for Healthcare Research and Quality (US); June 2013.

Shamliyan TA, Kane RL, Taylor FR. [Migraine in adults: Preventive pharmacologic treatments](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0056465/). Comparative Effectiveness Reviews No 103. Rockville (MD): Agency for Healthcare Research and Quality (US); April 2013.

**More information from PubMed Health:**

[Types of headache and signs of a migraine](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005091/)

[Migraine diary](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0041932/)

[Fact sheet for children and adolescents](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005003/)


