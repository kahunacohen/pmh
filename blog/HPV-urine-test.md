![Photo of women talking in a cafe](/core/assets/pmh/images/Women-cafe.jpg)

Could self-administered urine tests reduce the number of smear tests? A [systematic review of studies looking at this question](http://www.bmj.com/content/349/bmj.g5264) has been widely reported – and the [NHS Behind the Headlines team have analyzed](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2014-09-17-hpV-urine-test-could-screen-for-cervical-cancer/) the research results.

Human papilloma viruses (HPV) are common, and usually harmless. There are some types of HPV, though, that increase the risk of cervical cancer. Signs of that kind of HPV infection could be used to find out who might need further tests.

This new systematic review suggests that HPV urine tests “might be feasible for screening for cervical cancer,” according to the Behind the Headlines team. But there are still uncertainties. The experience of women in the different studies varied a lot, they said – and there were just under 1,500 women altogether.

Expect to see more research on this topic. And you can read more about HPV smear tests and cervical cancer screening generally in information from the Institute for Quality and Efficiency in Health Care [at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0050551/).

*The PubMed Health Team*

[Featured at Behind the Headlines on PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/)

[Read more about cervical cancer screening and HPV smear tests](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0050551/)


