![Photo of children brushing their teeth](/core/assets/pmh/images/Children-brushing-teeth.jpg)

People started using bristle toothbrushes first in the 18<sup>th</sup> century. Toothbrushes as we would recognize them today were developed in the 1930s. Powered toothbrushes came on the market in the 1960s. Today, there are many types, with new ones for sale all the time.

Electric toothbrushes mimic the motions used when we brush our teeth by hand, just much faster. Though they can be expensive, they are popular. But do we know which type of toothbrush works the best?

Researchers from the Cochrane Collaboration [studied this question](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011518/).\* They found dozens of short-term, small trials, comparing one type of powered toothbrush to a manual toothbrush.    

The most evidence is for rotation oscillation toothbrushes. This type first spins in one direction, and then the other, switching back and forth. Compared to manual toothbrushes, rotation oscillation brushes reduce both plaque and gingivitis more. This style of powered toothbrush has an advantage, but it is not yet clear how much of one.

That is because reduced [plaque](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0023263/) and [gingivitis](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0023280/) show up quickly. It takes a long time for them to cause [tooth decay](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0023279/) and [periodontitis](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0023281/). However, there are very few long-term studies on powered toothbrushes.

No matter what kind of toothbrush was used, brushing regularly reduced plaque. To read more about taking care of your toothbrush, [check out this page](http://www.cdc.gov/OralHealth/infectioncontrol/factsheets/toothbrushes.htm) from the Centers for Disease Control and Prevention (CDC).

Read more in the [researchers’ summary for consumers](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011518/) at PubMed Health.

*The PubMed Health Team*

**More information from PubMed Health**

[About the teeth](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0023261/)

[About gingivitis, including links to systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0023280/)

[About systematic reviews of evidence](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

\*Yaacob M, Worthington HV, Deacon SA, Deery C, Walmsley AD, Robinson PG, Glenny A. Powered versus manual toothbrushing for oral health. *Cochrane Database of Systematic Reviews* 2014, Issue 6. [[Free summary at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011518/)] [[Full text at The Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD002281.pub3/full)]

*Information on history of manual and powered toothbrushes from a 2012 [systematic review DE Slot and colleagues](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0050570/).*


