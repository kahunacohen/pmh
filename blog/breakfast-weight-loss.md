![Image of women eating a big breakfast.](/core/assets/pmh/images/Big-breakfast-blog-2013-08.jpg)

Is it true that eating a big breakfast is the secret to losing weight? You might think so after reading headlines about a study that women lost more weight when they ate their biggest meal of the day in the morning.

But as the team from [NHS Behind the Headlines](http://www.nhs.uk/news/2013/08August/Pages/Is-breakfast-the-most-important-meal-of-the-day.aspx) points out, the study focused on a specific group of overweight women—those with [metabolic syndrome](http://www.cancer.gov/dictionary?CdrID=643126), which increases the risk for coronary artery disease, stroke, and type 2 diabetes. The findings might not apply to other people. Also, the breakfast included foods such as tuna and whole-wheat bread, not bacon and eggs. The NHS analysis includes more information about the study and a link to the article.

If you need motivation to stick to your diet, catch up on some [evidence-based information from PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004993/) about the benefits of losing weight.

*The PubMed Health Team*


