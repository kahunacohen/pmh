![description of image](/core/assets/pmh/images/PMH_drugnames.jpg)

Drug names and classes
======================

A drug can have several names. There is usually a **generic** name for a drug substance plus one or more **brand** names. You can search for drugs by either their generic or brand names on PubMed Health. You can also search for drug classes.

**Generic names** for drugs are chosen by a variety of official bodies. That means that these names sometimes vary from country to country. For example, the generic name for one common pain medication is acetaminophen in the USA. However in many countries the same drug is called paracetamol.

Drug manufacturers choose the **brand names** of their products. There can be many brands of a particular drug. A brand name is also sometimes called a “proprietary” name.

The brand names are usually easier to say and easier to remember. They may be better known than the generic name. Acetaminophen is often called by one of its commonly used brands in the USA: Tylenol.

Drug classes
------------

A drug also belongs to one or more drug classes. A drug class is a group of drugs that have something in common. They are similar in some way, but they are not identical.

Drugs can be in a class with other drugs because:

-   The drugs are related by their chemical structure.

*Example:* Aspirin is a salicylate. Its full chemical name is “acetylsalicylic acid” or ASA. A salicylate is a chemical found in plants, for example, in willow tree bark and the meadowsweet plant.

-   The drugs work in the same way.

*Example:* Aspirin can prevent the formation of blood clots by stopping molecules in the blood called platelets from clumping or aggregating. So it belongs to a drug class called anti-platelets or platelet aggregation inhibitors.

-   The drugs are used for the same purpose.

*Example:* Aspirin is used to reduce fever. Drugs that treat fever are called anti-pyretic drugs or anti-pyretics.

Sometimes, we will have a page on a drug class. The drugs listed on those pages are ones for which we have information (not necessarily all of the drugs included in that class). 

<span>You can find information about searching for drugs on PubMed Health </span>[here](/pubmedhealth/finding-drug-information/)<span>.</span>

*By PubMed Health, 20 August 2015.*

[Approved drug uses](/pubmedhealth/approved-drug-uses/)

[Finding information about a drug](/pubmedhealth/finding-drug-information/)

[Tips about using medicines](/pubmedhealth/using-medicines/)


