PubMed Health Help
------------------

This section offers guidance on:

-   finding information in PubMed Health,
-   printing and sharing information,
-   linking to PubMed Health, and
-   citing PubMed Health pages.

Finding Information
-------------------

Health information and clinical effectiveness research can be found by using the browse options on the homepage or by performing a keyword search.

### Browse

-   The Contents section on the homepage allows you to browse our complete A-Z list of titles for each type of information and research in PubMed Health.
-   Synonyms are listed for complex medical terms to help you locate the most relevant information.

### Keyword Search

-   The search box appears on every PubMed Health page.
-   "Refine your search" (an option at the top of every search results page) allows you to narrow your search results to one of the main types of information and research (such as full texts of clinical effectiveness reviews).
-   Search results provide information on when the article was created, updated, or last reviewed.
-   On the right side, search results within our medical encyclopedia information are shown, as well as simultaneous search results in PubMed Health for systematic reviews.

Can’t Find What You’re Looking For?
-----------------------------------

PubMed Health is committed to providing current information to answer many health related questions. You may also search [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/) for abstracts of biomedical literature. If you are seeking additional full-text articles, please visit [PubMed Central](http://www.ncbi.nlm.nih.gov/pmc/).

Printing and sharing information
--------------------------------

As well as the special printing buttons, another printing button is included in the "Share" section to the right of the search box at the top of every page.

As well as printing, you can email to a friend here, or post directly into your online social networks.

How to Cite PubMed Health Pages
-------------------------------

If you are citing an individual page on PubMed Health, the National Library of Medicine recommends the citation style provided in [Citing Medicine: The NLM Style Guide for Authors, Editors, and Publishers, 2nd edition](http://www.ncbi.nlm.nih.gov/books/NBK7256/).

### Homepage

PubMed Health [Internet]. Bethesda (MD): National Library of Medicine (US); [updated 2011 Jan 1; cited 2011 Jan 6]. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/>

### <span style="font-size: 16.8px; line-height: 20.4px;">Agency for Healthcare Research and Quality (US)</span>

Begin by citing the author and title, then add information about the entry being cited. Examples:

Eisenberg Center at Oregon Health & Science University. Having a Breast Biopsy: A Guide for Women and Their Familes [Internet]. Rockville (MD): Agency for Healthcare Research and Quality (US); 2010 Apr 14 [cited 2011 Jan 26]. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004896/>

Williams JW, Plassman BL, Burke J, Holsinger T, Benjamin S. Preventing Alzheimer's Disease and Cognitive Decline [Internet]. Rockville (MD): Agency for Healthcare and Research Quality (US); 2010 Apr [cited 2011 Jul 15]. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0008821>

### Canadian Agency for Drugs and Technologies in Health

Begin by citing the author and title, then add information about the entry being cited. Examples:

Ho C, Cimon K, Rabb D. Transcatheter Aortic Valve Replacement in Severe Aortic Stenosis: A Review of Comparative Durability and Clinical Effectiveness Beyond 12 Months [Internet]. Ottawa (ON): Canadian Agency for Drugs and Technologies in Health; 2013 Apr [cited 2013 Dec 23]. (Rapid Response Report: Peer-reviewed summary with critical appraisal.) Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0060349/>

Canadian Agency for Drugs and Technologies in Health. Management of Relapsing-Remitting Multiple Sclerosis [Internet]. Ottawa (ON): Canadian Agency for Drugs and Technologies in Health; 2013 Oct [cited 2013 Dec 23]. (CADTH Therapeutic Review, No. 1.2.) Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0060855/>

### Centre for Reviews and Dissemination Database of Abstracts of Reviews of Effects (DARE)

Begin by citing the DARE database, then add information about the review being cited. For example:

Database of Abstracts of Reviews of Effects (DARE): Quality-assessed Reviews [Internet]. York (UK): Centre for Reviews and Dissemination; c2011. Role of adjuvant chemotherapy in patients with resected non-small-cell lung cancer: reappraisal with a meta-analysis of randomized controlled trials (review); 2004 [cited 2011 Nov 28]. Available from: [https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0021715/](/pubmedhealth/PMH0021715/)

### Cochrane Database of Systematic Reviews: Plain Language Summaries

Begin by citing the author and title of the summary, then add information about the summary being cited. For example:

Uman LS, Chambers CT, McGrath PJ, Kisely S. Psychological interventions for needle-related procedural pain and distress in children and adolescents [review]. Cochrane Database of Systematic Reviews: Plain Language Summaries [Internet]. 2006 Oct 18 [cited 2011 Dec 8];(4):CD005179. Available from <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0013300/>

### German Institute for Quality and Efficiency in Health Care (IQWiG) content

Begin by citing the title, then add information about the article being cited. For example:

How are blood stem cells obtained for transplantation? [Internet]. Cologne (DE): German Institute for Quality and Efficiency in Health Care; 2010 July 1 [updated 2013 Nov 11; cited 2015 Oct 16]. Available from: [https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005173/](/pubmedhealth/PMH0005173/)

### Micromedex Consumer Medication Information

Begin by citing the Micromedex Consumer Medication Information database, then add information about the drug being cited. For example:

Micromedex Consumer Medication Information [Internet]. Ann Arbor (MI): Truven Health Analytics; c2012. Aspirin (By mouth); 2015 Oct 1 [cited 2015 Oct 16]. Available from: [https://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025714/?report=details ](https://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025714/?report=details%20)

### National Cancer Institute PDQ Cancer Information Summaries

Begin by citing the NCI resource, then add information about the entry being cited. For example:

PDQ Cancer Information Summaries [Internet]. Bethesda (MD): National Cancer Institute (US). [date unknown]. Breast Cancer Prevention (PDQ®). Patient version; [updated 2015 Jul 21; cited 2015 Oct 16]. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0032652/>

### National Health Service (NHS) Behind the Headlines from NHS Choices

Invasive early prostate cancer treatments not always needed. NHS Behind the Headlines. 2016 Sep 15 [cited 2016 Sep 26]. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2016-09-15-invasive-early-prostate-cancer-treatments-not-always-needed/>

### National Heart, Lung, and Blood Institute (NHLBI) Health Topics

NHLBI Health Topics [Internet]. Bethesda (MD): National Heart, Lung, and Blood Institute (US). [date unknown]. Arrhythmia; [updated 2014 Feb 6; cited 2014 Feb 18]. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0062940/>

### NHS National Institute for Health Research (NIHR) Health Technology Assessment Programme

Begin by citing the author(s) and title of the NIHR HTA content, then add information about the entry being cited. Volume, issue and page numbers of the executive summary being cited can be found in the provided PDF. For example:

Pandor A, Goodacre S, Harnan S, Holmes M, Pickering A, Fitzgerald P, Rees A, Stevenson M. Diagnostic management strategies for adults and children with minor head injury: a systematic review and an economic evaluation [executive summary]. Health Technol Assess [Internet]. 2011 [cited 2011 Sept 14];15(27): ii-v. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0015186/>

### National Institute for Health and Clinical Excellence (NICE) guidelines

Begin by citing the author(s) and title of the guideline. Then add information about the guideline being cited. For example:

National Collaborating Centre for Chronic Conditions (UK). Osteoarthritis: National Clinical Guideline for Care and Management in Adults [Internet]. London: Royal College of Physicians (UK); 2008 [cited 2011 July 15]. Available from: [https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0008692/](/pubmedhealth/PMH0008692/)

You can identify the author and publisher of NICE guidelines by clicking on the cover thumbnail of the guideline.

### Oregon Health & Science University Drug Class Reviews (DERP)

Begin by citing the author(s) and title of the Drug Class Review, then add information about the review being cited. For example:

Gartlehner G, Hansen RA, Reichenpfader U, Kaminski A, Kien C, Strobelberger M, Van Noord M, Thieda P, Thaler K, Gayness B.  Drug Class Review: Second-Generation Antidepressants: Final Update 5 Report [Internet].  Portland (OR): Oregon Health & Science University; 2011 Mar [cited 2011 Jul 15].  Available from <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0009808/>

### PubMed Clinical Q&A content

Begin by citing the PubMed Clinical Q&A material, then add information about the entry being cited. For example:

Dean L. Comparing NSAIDs. 2007 Dec 1 [cited 2011 Jan 26]. In: PubMed Health [Internet]. Bethesda (MD): National Library of Medicine (US); 2011-. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004928/>

### Swedish Council on Health Technology Assessment (SBU)

<span style="line-height: 20.4px;">Begin by citing the author and title, then add information about the entry being cited. For example:</span>

Swedish Council on Health Technology Assessment. Transient Elastography with Suspected Fibrosis and Cirrhosis of the Liver (Summary and conclusions) [Internet]. Stockholm: Swedish Council on Health Technology Assessment (SBU); 2013 Mar 13. SBU Alert Report No.: 2013-01. Available from: [https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0078710/](https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0078710/%20)

### Veterans Affairs' Evidence-based Synthesis Program (VA ESP)

Begin by citing the author(s) and title of the VA material, then add information about the entry being cited. For example:

Bean-Mayberr B, Huang C, Batuman F, Goldzweig C, Washington DC. Yano EM, Miake-Lye I.  Systematic Review of Women Veterans Health Research 2004-2008 [Internet]. Washington (DC): Department of Veterans Affairs (US); 2010 Oct cited 2011 Jul 15]. Available from: <https://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0009116/>

Contact Us
----------

To send suggestions, recommendations for new resources and/or updates to existing information, email us at <pmh-help@ncbi.nlm.nih.gov>.


