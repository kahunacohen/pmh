![description of image](/core/assets/pmh/images/PMH_manpill.jpg)

Using medicines: things you need to know
========================================

To find information on a specific drug or brand name, please use the search box at the top of the page. Or go to our page of tips on finding more information [here](/pubmedhealth/finding-drug-information/).
 The articles below come from a variety of public agencies that are interested in helping you get the best out of your medicines.

Medicines: basics
-----------------

![Image of doctor](/core/assets/pmh/images/PMH_doctor.jpg)

-   [Approved Drug Uses](/pubmedhealth/approved-drug-uses/)
-   [Drug names and classes](/pubmedhealth/drug-names-and-classes)

Drugs & supplements:
 long term use
--------------------

![whatever](/core/assets/pmh/images/PMH_drugsupp.jpg)

-   [Managing medicines long term](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004994/)
-   [Dietary supplements](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004991/)

Side effects
------------

![Image of boy taking medicine](/core/assets/pmh/images/PMH_childpill.jpg)

-   [Reporting side effects to the FDA](/pubmedhealth/FDA-reporting/)
-   [Adverse effects: knowing more](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016267/)

Using antibiotics
-----------------

![Image of pills](/core/assets/pmh/images/PMH_pills.jpg)

-   [Tips: using antibiotics correctly](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005149/)
-   [In depth: safe use of antibiotics](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005150/)

[Approved drug uses](/pubmedhealth/approved-drug-uses/)

[Drug names and classes](/pubmedhealth/drug-names-and-classes/)

[Finding information about a drug](/pubmedhealth/finding-drug-information/)


