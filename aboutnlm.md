<span id="breadcrumb_text">[Home](/) \> Help</span>

About National Library of Medicine
----------------------------------

The U.S. [National Library of Medicine](http://www.nlm.nih.gov/about/index.html) (NLM) is the world's largest medical library. It has millions of books and journals about all aspects of medicine and health care on its shelves. Its electronic services deliver trillions of bytes of data to millions of users every day.

The NLM was founded in 1836 and is part of the [National Institutes of Health](http://www.nih.gov) (NIH) in Bethesda, Maryland.
  
  

[How do we know what works](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)

[How to read health news](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/how-to-read/)


