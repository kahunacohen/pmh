INFORMED HEALTH ONLINE: CURRENT MEDICAL KNOWLEDGE
=================================================

The articles and illustrations from Informed Health Online are based on the best evidence that was available at the time of publication.

To make sure that our information is always up-to-date, we regularly check it and – if necessary – update it. Below each article you will find the date when it was last updated.

Extensive literature searches form the foundation of our articles. When looking for answers to questions on the benefits and harms of medical interventions, we mainly use systematic reviews.

Draft versions of our articles are reviewed by experts within our institute, as well as by external experts, and also tested by users. But the final contents of the articles are determined by IQWiG employees alone.

If you would like to know more about our institute, our methods and processes, our methods are also described in the [published methods](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0082853/) of the Institute for Quality and Efficiency in Health Care (IQWiG, Germany).

Quality Assurance
-----------------

The process of producing our information usually takes several months. During this time, our draft versions are reviewed internally and externally several times.

*Internal peer review*

Experts from other IQWiG departments are also involved in the production of our information. For example, they assure the quality of the search for suitable literature, as well as the selection and evaluation of the systematic reviews. The experts in our institute include doctors, health-care researchers and scientists, as well as statisticians and specialists in the fields of psychology and the social sciences.

*External peer review*

We send drafts of our information to independent experts who are specialized in the topic area. We review their comments internally, and make improvements to our draft versions accordingly.

The members of IQWiG’s Board of Trustees are also invited to comment on our information before we publish it.

*Testing by users*

Alongside the peer reviews, each article is read by people who are often personally affected by the medical condition in question. This “testing by users” takes place at the Hannover Medical School (MHH, Germany). We edit and improve our information based on users’ feedback. Although we are, of course, not able to please each and every reader, we try to take as many comments into consideration as possible.

[*Sources*](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0072374/)

<span>© IQWiG (Institute for Quality and Efficiency in Health Care)</span>


