![Image of man reading on a tablet device](/core/assets/pmh/images/PMH_research.jpg)

Understanding research results
==============================

Studies, numbers and health claims—we're flooded with them every day. Don't be misled or confused. Use the lively and interesting books on this page to make sense of it all.
 One of these books has even been clinically proven to increase readers' data interpretation skills! See the [Know Your Chances randomized trials](http://www.ncbi.nlm.nih.gov/pubmed/17310049%20).

The books are all complete and online free here.

  

Know your chances
-----------------

![Know Your Chances book cover](/core/assets/pmh/images/know_your_chances.jpg)

-   [Understanding health statistics](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0050876/)
-   How to see through hype

Testing treatments
------------------

![Testing Treatments book cover](/core/assets/pmh/images/testing_treatments_2.jpg)

-   [Better research for better healthcare](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0050892/)
-   How we can know if a treatment works

Smart health choices
--------------------

![Smart Health Choices book cover](/core/assets/pmh/images/smart_health_choices.jpg)

-   [Making sense of health advice](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016239/)
-   Develop the skills for informed decisions

[How to read health news](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/how-to-read/)

[What to know about screening](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0033150/)

[Why randomization is important](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005079/)


